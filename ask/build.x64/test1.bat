@setlocal
@set TMPEXE=Release\ask.exe
@if NOT EXIST %TMPEXE% goto NOASK
@REM ask -? >nul 2>&1
@REM if ERRORLEVEL 1 goto NOASK
@REM set TMPEXE=ask

:FNDASK
@echo Testing %TMPEXE% ...
@echo.

@%TMPEXE% Test 1: Enter a Y/y ...
@call :CHKERR

@echo.

@%TMPEXE% Test  2: With a big prompt ... Do you want to CONTINUE ... enter y or something...
@call :CHKERR

@REM UGH! Seems using _kbhit() and _getch does NOT work for redirection!!!
@REM What changed... I am sure it used to??? Or maybe it NEVER worked
@REM AHA, seems to get from stdin, ie get from redirection, must use getchar
@REM UGH: But getchar requires an <enter> before it returns, AND it echos to the console, so
@REM NO REDIRECTIONS!!!
@REM goto DONE

@echo.
@echo Testing automation... ie input from a file - first a 'y'
@echo y > temp.txt

@%TMPEXE% < temp.txt
@call :CHKERR

@echo.
@echo Testing automation... ie input from a file - next a 'n'
@echo n > temp.txt

@%TMPEXE% < temp.txt
@call :CHKERR


:DONE
@echo.
@echo Testing of %TMPEXE% all done...
@goto END

:NOASK
@echo.
@echo Can NOT find a local Release\ask.exe
@REM echo Nor is ask in the PATH
@echo.
@goto END

:CHKERR
@if ERRORLEVEL 1 (
@echo.
@echo Got YES ...
@echo.
) else (
@echo.
@echo *** DID NOT GET A YES ***
@echo.
)


:END

