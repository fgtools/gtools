file: README.txt - 20140221 - 20130730

A simple utility program to 'ask' to continue... Something 
like pause in DOS

Usage:
 ask any prompt [-v]
@if ERRORLEVE 2 (
  @echo ask failed! Not in PATH
) else if ERRORLEVEL 1 (
  @echo got a 'y' reply
) else (
  @echo got input other than 'y'
)

Released under BSD 3 clause licence - see LICENCE.txt

Building:

This is a cmake project - see CMakeLists.txt

In Unix:
$ cd build
$ cmake .. [OPTIONS]
$ make
$ optional: [sudo] make install

In windows if MSVC has been installed
> cd build
> cmake .. [OPTIONS]
> cmake --build . --config Release
> optional: cmake --build . --config --target INSTALL
And of course the config can be Debug, or others.

Some batch files and shell scripts are provide to do the 
above. See build-me.bat and build-me.sh

Enjoy,
Geoff.

# eof

