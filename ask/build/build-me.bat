@setlocal
@set TMPLOG=bldlog-1.txt
@set TMPOPTS=-DCMAKE_INSTALL_PREFIX=C:\MDOS

:RPT
@if "%~1x" == "x" goto GOTOPTS
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT

:GOTOPTS
@echo Bgn %DATE% %TIME% > %TMPLOG%

cmake .. %TMPOPTS% >> %TMPLOG%
@if ERRORLEVEL 1 goto ERR1

cmake --build . --config Release >> %TMPLOG%
@if ERRORLEVEL 1 goto ERR2

@echo.
@echo Appears a successfile build...
@echo.
@echo Continue to install ask32.exe to C:\MDOS?
@echo.
@pause

cmake --build . --config Release --target INSTALL >> %TMPLOG%

@fa4 " -- " %TMPLOG%

@echo Done build and install of ask32...

@goto END

:Err1
@echo Error: cmake config or gen FAILED!
@goto END

:Err2
@echo Error: cmake compile or link FAILED!
@goto END

:END

@REM eof



