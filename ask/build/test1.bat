@setlocal
@set TMPEXE=Release\ask32.exe
@if NOT EXIST %TMPEXE% goto NOASK
@REM ask -? >nul 2>&1
@REM if ERRORLEVEL 1 goto NOASK
@REM set TMPEXE=ask

:FNDASK
@echo Testing %TMPEXE% ...
@echo.

@%TMPEXE% Test 1: Enter a Y/y ...
@if ERRORLEVEL 1 (
@echo Got YES ...
) else (
@echo DID NOT GET A YES ...
)

@echo.

@%TMPEXE% Test  2: With a big prompt ... Do you want to CONTINUE ... enter y or something...
@if ERRORLEVEL 1 (
@echo Got YES
) else (
@echo DID NOT GET A YES ...
)

@REM UGH! Seems using _kbhit() and _getch does NOT work for redirection!!!
@REM What changed... I am sure it used to??? Or maybe it NEVER worked
@goto DONE

@echo.
@echo Testing automation... ie input from a file
@echo y > temp.txt

@%TMPEXE% < temp.txt
@if ERRORLEVEL 1 (
@echo Got a Y
) else (
@echo DID NOT GET A YES ...
)

:DONE
@echo.
@echo Testing of %TMPEXE% all done...
@goto END

:NOASK
@echo.
@echo Can NOT find a local Release\ask.exe
@echo Nor is ask in the PATH
@echo.
@goto END

:END

