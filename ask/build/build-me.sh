#!/bin/sh
#< build-me.sh - ask project - 20130730
BN=`basename $0`

echo ""
echo "$BN: Some suggested cmake options to use for debug..."
echo "  -DCMAKE_VERBOSE_MAKEFILE=TRUE - use a verbose Makefile good to see flags. switches, libraries, etc..."
echo "  -DCMAKE_BUILD_TYPE=DEBUG - to add symbols for gdb use (add -g compiler switch)"
echo "  Then run gdb with '\$ gdb --args ask prompt'"
echo "  -DCMAKE_INSTALL_PREFIX:PATH=$HOME - to add a spcific install location"
echo "  This will be added if no command is given"
echo ""

TMPOPTS=""
for arg in $@; do
    TMPOPTS="$TMPOPTS $arg"
done

TMPMSG=""
if [ -z "$TMPOPTS" ]; then
    TMPOPTS="-DCMAKE_INSTALL_PREFIX:PATH=$HOME"
    TMPMSG="To install to '$HOME/bin' unless otherwise directed"
fi

echo "$BN: Doing 'cmake .. $TMPOPTS'..."
cmake .. $TMPOPTS
if [ ! "$?" = "0" ]; then
    echo "$BN: Have configuration, generation error"
    exit 1
fi

echo ""
echo "$BN: Doing 'make'"
make
if [ ! "$?" = "0" ]; then
    echo "$BN: Have compile, link error"
    exit 1
fi

echo ""
echo "$BN: appears successful..."
echo ""
echo "$BN: maybe '[sudo] make install' next?"
echo "$TMPMSG"

# eof

