
// stat64.cpp : Defines the entry point for the console application.

// use: To get the stat64 information for a file, or files,
// and idsplay it to the console.
// It is particularly designed to check if it is a 'sparse' file,
// and if it is, show the TRUE data size ...

// Is encode in UNICODE, and has a service to convert UNICODE to ASCII
// Uses both say 'printf(char *, ...), and wprintf( wchar_t *, ...),
// using the _L("text") macro to create UNICODE text strings.
/* ---------------------------------------------------------------------------------------------
   NOTES OF FILE SIZE versus SIZE ON DISK
   It depends on the sector and clustor sizes, whihc depend on the drive size
   from : http://technet.microsoft.com/en-us/library/cc781134(v=ws.10).aspx
   Volume Size	             NTFS Cluster Size
   7 megabytes (MB)�512 MB   512 bytes
   513 MB�1,024 MB           1 KB
   1,025 MB�2 GB             2 KB
   2 GB�2 terabytes          4 KB
   But this value can be set when formatting a drive!!!

   --------------------------------------------------------------------------------------------- */
// 2009-05-13 - added -s = show size only, and -t = show other times, if different
#ifndef _CRT_NON_CONFORMING_SWPRINTFS
#define _CRT_NON_CONFORMING_SWPRINTFS
#endif
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include "stat64.h"
//#include <FileExtd.h>
//#include <WinBase.h>

#define  VFH(a)   ( a && ( a != INVALID_HANDLE_VALUE ) )

#ifdef UNICODE
#define  STAT   _wstat64
#define  PRINTF wprintf
#define  SPRINTF swprintf
#define  STRLEN wcslen
#define  STRCAT wcscat
#define  STRCPY wcscpy
#else
#define  STAT   _stat64
#define  PRINTF printf
#define  SPRINTF sprintf
#define  STRLEN strlen
#define  STRCAT strcat
#define  STRCPY strcpy
#endif

static int utc_option = 1;
static int raw_option = 0;
static int tim_option = 0;
static int verb_on = 0;
static int filcount = 0;

#define VERB1 (verb_on >= 1)
#define VERB2 (verb_on >= 2)
#define VERB5 (verb_on >= 5)
#define VERB9 (verb_on >= 9)


// forward ref
_TCHAR * get_next_buffer( void );

static _TCHAR power_letter[] =
{
  0,	/* not used */
  'K',	/* kibi ('k' for kilo is a special case) */
  'M',	/* mega or mebi */
  'G',	/* giga or gibi */
  'T',	/* tera or tebi */
  'P',	/* peta or pebi */
  'E',	/* exa or exbi */
  'Z',	/* zetta or 2**70 */
  'Y'	/* yotta or 2**80 */
};

void give_help( void )
{
   printf( "usage: stat64 [options] file-name [file_name2 [file_name3 ...]]\n" );
   printf( "options: Prefixed with '-' ... space spearated.\n" );
   printf( " -? or -h = This brief help.\n" );
   printf( " -u       = Show UTC times. (default)\n" );
   printf( " -l       = Show local times.\n" );
   printf( " -s       = Out only the RAW file size.\n" );
   printf( " -t       = Show other times, if different.\n" );
   printf( " -v[v...] = Bump verbosity. (def=%d)\n", verb_on );
   printf( " -f=file  = Stat this file. Usefull if name starts with '-'.\n" );
   printf( "Compiled on " __DATE__ " at " __TIME__ "\n" );

}

void nice_num( _TCHAR * dst, _TCHAR * src ) // get nice number, with commas
{
   size_t i;
   size_t len = STRLEN(src);
   size_t rem = len % 3;
   size_t cnt = 0;
   for( i = 0; i < len; i++ )
   {
      if( rem ) {
         *dst++ = src[i];
         rem--;
         if( ( rem == 0 ) && ( (i + 1) < len ) )
            *dst++ = ',';
      } else {
         *dst++ = src[i];
         cnt++;
         if( ( cnt == 3 ) && ( (i + 1) < len ) ) {
            *dst++ = ',';
            cnt = 0;
         }
      }
   }
   *dst = 0;
}


_TCHAR * get_nice_number( __int64 num, size_t width )
{
   size_t len, i;
   _TCHAR * pb1 = get_next_buffer();
   _TCHAR * pb2 = get_next_buffer();
   SPRINTF(pb1, _T("%I64d"), num);
   nice_num( pb2, pb1 );
   len = STRLEN(pb2);
   if( len < width )
   {
      *pb1 = 0;
      i = width - len;  // get pad
      while(i--)
         STRCAT(pb1, _T(" "));
      STRCAT(pb1,pb2);
      pb2 = pb1;
   }
   return pb2;
}

// convert UNICODE to UTF-8 (ASCII) = WideCharToMultiByte
// conversion of UNICODE to CP_UTF8 characters = WideCharToMultiByte
#define  DEF_CP   CP_UTF8
int Wide2UTF8( _TCHAR * pFile, char * ps, int len ) // = WideCharToMultiByte
{
   int wlen = (int)STRLEN(pFile);
   int ret, i;
   for( i = 0; i < len; i++ )
      ps[i] = 0;
      
   ret = WideCharToMultiByte( DEF_CP, // UINT CodePage,            // code page
      0, // DWORD dwFlags,            // performance and mapping flags
      pFile,   // LPCWSTR lpWideCharStr,    // wide-character string
      wlen,     // int cchWideChar,          // number of chars in string.
      ps,      // LPSTR lpMultiByteStr,     // buffer for new string
      len,    // int cbMultiByte,          // size of buffer
      NULL,    // LPCSTR lpDefaultChar,     // default for unmappable chars
      NULL );  // LPBOOL lpUsedDefaultChar  // set when default char used
   return ret;
}

// convert UTF-8 to UNICODE 
int UTF82Wide( char * ps, int len, _TCHAR * pWide, int wlen )
{
   int iret = MultiByteToWideChar(
      CP_UTF8, // UINT CodePage,         // code page
      0, // MB_PRECOMPOSED, // DWORD dwFlags,  // character-type options
      ps,      // LPCSTR lpMultiByteStr, // string to map
      len,     // int cbMultiByte,       // number of bytes in string
      pWide,   // LPWSTR lpWideCharStr,  // wide-character buffer
      wlen );  // int cchWideChar        // size of buffer
   if(iret == 0) {
      DWORD dwi = GetLastError();
      printf( "ERROR: Translation to UNICODE failed: (%d) ", dwi );
      //DisplayErrorText( dwi );
   }
   return iret;
}

_TCHAR * get_stmp_buf(void)
{
   static _TCHAR _s_cTmpBuf[MAX_PATH];
   return _s_cTmpBuf;
}

void Convert_2_ASCII( LPCWSTR pWide )
{
   _TCHAR * ptmp = get_stmp_buf();
   Wide2UTF8( (PTSTR)pWide, (char *)ptmp, MAX_PATH ); // = WideCharToMultiByte
   strcpy((char *)pWide, (char *)ptmp);
}

void Convert_2_UNICODE( char * ps )
{
   _TCHAR * ptmp = get_stmp_buf();
   int len = (int)strlen(ps);
   UTF82Wide( ps, len, ptmp, MAX_PATH );
   wcscpy((wchar_t *)ps, ptmp);
}

void trim_float_buf( _TCHAR * pb )
{
   size_t len = STRLEN(pb);
   size_t i, dot, zcnt;
   for( i = 0; i < len; i++ )
   {
      if( pb[i] == '.' )
         break;
   }
   dot = i + 1; // keep position of decimal point (a DOT)
   zcnt = 0;
   for( i = dot; i < len; i++ )
   {
      if( pb[i] != '0' )
      {
         i++;  // skip past first
         if( i < len )  // if there are more chars
         {
            i++;  // skip past second char
            if( i < len )
            {
               size_t i2 = i + 1;
               if( i2 < len )
               {
                  if ( pb[i2] >= '5' )
                  {
                     if( pb[i-1] < '9' )
                     {
                        pb[i-1]++;
                     }
                  }
               }
            }
         }
         pb[i] = 0;
         break;
      }
      zcnt++;     // count ZEROS after DOT
   }
   if( zcnt == (len - dot) )
   {
      // it was ALL zeros
      pb[dot - 1] = 0;
   }
}

_TCHAR * get_k_num( __int64 i64, int ascii, int type )
{
   int dotrim = 1;
   _TCHAR * pb = get_next_buffer();
   _TCHAR * form = _T(" bytes");
   unsigned __int64 byts = i64;
   double res;
   _TCHAR * ffm = _T("%0.20f");  // get 20 digits
   if( byts < 1024 ) {
      SPRINTF(pb, _T("%I64u"), byts);
      dotrim = 0;
   } else if( byts < 1024*1024 ) {
      res = ((double)byts / 1024.0);
      form = (type ? _T(" KiloBypes") : _T(" KB"));
      SPRINTF(pb, ffm, res);
   } else if( byts < 1024*1024*1024 ) {
      res = ((double)byts / (1024.0*1024.0));
      form = (type ? _T(" MegaBypes") : _T(" MB"));
      SPRINTF(pb, ffm, res);
   } else { // if( byts <  (1024*1024*1024*1024)){
      double val = (double)byts;
      double db = (1024.0*1024.0*1024.0);   // x3
      double db2 = (1024.0*1024.0*1024.0*1024.0);   // x4
      if( val < db2 )
      {
         res = val / db;
         form = (type ? _T(" GigaBypes") : _T(" GB"));
         SPRINTF(pb, ffm, res);
      }
      else
      {
         db *= 1024.0;  // x4
         db2 *= 1024.0; // x5
         if( val < db2 )
         {
            res = val / db;
            form = (type ? _T(" TeraBypes") : _T(" TB"));
            SPRINTF(pb, ffm, res);
         }
         else
         {
            db *= 1024.0;  // x5
            db2 *= 1024.0; // x6
            if( val < db2 )
            {
               res = val / db;
               form = (type ? _T(" PetaBypes") : _T(" PB"));
               SPRINTF(pb, ffm, res);
            }
            else
            {
               db *= 1024.0;  // x6
               db2 *= 1024.0; // x7
               if( val < db2 )
               {
                  res = val / db;
                  form = (type ? _T(" ExaBypes") : _T(" EB"));
                  SPRINTF(pb, ffm, res);
               }
               else
               {
                  db *= 1024.0;  // x7
                  res = val / db;
                  form = (type ? _T(" ZettaBypes") : _T(" ZB"));
                  SPRINTF(pb, ffm, res);
               }
            }
         }
      }
   }
   if( dotrim > 0 )
      trim_float_buf(pb);

   STRCAT(pb, form);

   if( ascii > 0 )
      Convert_2_ASCII(pb);

   return pb;
}

_TCHAR * get_k_num_with_2_sig_chars( __int64 i64, int ascii, int type )
{
   _TCHAR * pb = get_next_buffer();
   _TCHAR * form = _T("%I64u bytes");
   unsigned __int64 byts = i64;
   double res;
   if( byts < 1024 ) {
      SPRINTF(pb, form, byts);
   } else if( byts < 1024*1024 ) {
      res = ((double)byts / 1024.0);
      form = (type ? _T("%0.2f KiloBypes") : _T("%0.2f KB"));
      SPRINTF(pb, form, res);
   } else if( byts < 1024*1024*1024 ) {
      res = ((double)byts / (1024.0*1024.0));
      form = (type ? _T("%0.2f MegaBypes") : _T("%0.2f MB"));
      SPRINTF(pb, form, res);
   } else { // if( byts <  (1024*1024*1024*1024)){
      double val = (double)byts;
      double db = (1024.0*1024.0*1024.0);   // x3
      double db2 = (1024.0*1024.0*1024.0*1024.0);   // x4
      if( val < db2 )
      {
         res = val / db;
         form = (type ? _T("%0.2f GigaBypes") : _T("%0.2f GB"));
         SPRINTF(pb, form, res);
      }
      else
      {
         db *= 1024.0;  // x4
         db2 *= 1024.0; // x5
         if( val < db2 )
         {
            res = val / db;
            form = (type ? _T("%0.2f TeraBypes") : _T("%0.2f TB"));
            SPRINTF(pb, form, res);
         }
         else
         {
            db *= 1024.0;  // x5
            db2 *= 1024.0; // x6
            if( val < db2 )
            {
               res = val / db;
               form = (type ? _T("%0.2f PetaBypes") : _T("%0.2f PB"));
               SPRINTF(pb, form, res);
            }
            else
            {
               db *= 1024.0;  // x6
               db2 *= 1024.0; // x7
               if( val < db2 )
               {
                  res = val / db;
                  form = (type ? _T("%0.2f ExaBypes") : _T("%0.2f EB"));
                  SPRINTF(pb, form, res);
               }
               else
               {
                  db *= 1024.0;  // x7
                  res = val / db;
                  form = (type ? _T("%0.2f ZettaBypes") : _T("%0.2f ZB"));
                  SPRINTF(pb, form, res);
               }
            }
         }
      }
   }

   if( ascii > 0 )
      Convert_2_ASCII(pb);

   return pb;
}

/* -------------------------------------------------
BOOL WINAPI GetVolumeInformationByHandleW(
  _In_       HANDLE hFile,
  _Out_opt_  LPWSTR lpVolumeNameBuffer,
  _In_       DWORD nVolumeNameSize,
  _Out_opt_  LPDWORD lpVolumeSerialNumber,
  _Out_opt_  LPDWORD 
     lpMaximumComponentLength,
  _Out_opt_  LPDWORD lpFileSystemFlags,
  _Out_opt_  LPWSTR lpFileSystemNameBuffer,
  _In_       DWORD nFileSystemNameSize
);

typedef struct _FILE_ID_BOTH_DIR_INFO {
  DWORD         NextEntryOffset;
  DWORD         FileIndex;
  LARGE_INTEGER CreationTime;
  LARGE_INTEGER LastAccessTime;
  LARGE_INTEGER LastWriteTime;
  LARGE_INTEGER ChangeTime;
  LARGE_INTEGER EndOfFile;
  LARGE_INTEGER AllocationSize;
  DWORD         FileAttributes;
  DWORD         FileNameLength;
  DWORD         EaSize;
  CCHAR         ShortNameLength;
  WCHAR         ShortName[12];
  LARGE_INTEGER FileId;
  WCHAR         FileName[1];
} FILE_ID_BOTH_DIR_INFO, *PFILE_ID_BOTH_DIR_INFO;

FileInformationClass value	lpFileInformation type
FileBasicInfo (0)	                   FILE_BASIC_INFO
FileStandardInfo (1)	               FILE_STANDARD_INFO
FileNameInfo (2)	                   FILE_NAME_INFO
FileStreamInfo (7)	                   FILE_STREAM_INFO
FileCompressionInfo (8)	               FILE_COMPRESSION_INFO
FileAttributeTagInfo (9)	           FILE_ATTRIBUTE_TAG_INFO
FileIdBothDirectoryInfo (0xa)	       FILE_ID_BOTH_DIR_INFO
FileIdBothDirectoryRestartInfo (0xb)   FILE_ID_BOTH_DIR_INFO
FileRemoteProtocolInfo (0xd)	       FILE_REMOTE_PROTOCOL_INFO
FileFullDirectoryInfo (0xe)	           FILE_FULL_DIR_INFO
FileFullDirectoryRestartInfo (0xf)	   FILE_FULL_DIR_INFO
FileStorageInfo (0x10)	               FILE_STORAGE_INFO
FileAlignmentInfo (0x11)	           FILE_ALIGNMENT_INFO
FileIdInfo (0x12)	                   FILE_ID_INFO
FileIdExtdDirectoryInfo (0x13)	       FILE_ID_EXTD_DIR_INFO
FileIdExtdDirectoryRestartInfo (0x14)  FILE_ID_EXTD_DIR_INFO

BOOL WINAPI GetFileInformationByHandleEx(
  _In_   HANDLE hFile,
  _In_   FILE_INFO_BY_HANDLE_CLASS FileInformationClass,
  _Out_  LPVOID lpFileInformation,
  _In_   DWORD dwBufferSize
);

typedef struct _FILE_STANDARD_INFO {
  LARGE_INTEGER AllocationSize;
  LARGE_INTEGER EndOfFile;
  DWORD         NumberOfLinks;
  BOOLEAN        DeletePending;
  BOOLEAN       Directory;
} FILE_STANDARD_INFO, *PFILE_STANDARD_INFO;

   ------------------------------------------------- */
#define MY_MX_BUF_SIZE 1024
#define MY_MX_PATH (MAX_PATH + 2)

typedef struct tagGETVOLINFO {
    _TCHAR VolName[MY_MX_PATH];
    DWORD VolSerial;
    DWORD MaxComLen;
    DWORD FSFlag;
    _TCHAR FSName[MY_MX_PATH];
}GETVOLINFO, *PGETVOLINFO;

static GETVOLINFO vi;
static BOOL GotVolInfo;
static _TCHAR fibuf[MY_MX_BUF_SIZE];
static BOOL GotAllocSize;
int win_is_sparse_file( _TCHAR * file )
{
   int iret = 0;
   //PGETVOLINFO pvi = &vi;
   //_TCHAR *pf = (file[1] == ':') ? file : 0;
   //GotVolInfo = GetVolumeInformation( pf,
   //        pvi->VolName,
   //        MY_MX_PATH,
   //        &pvi->VolSerial,
   //        &pvi->MaxComLen,
   //        &pvi->FSFlag,
   //        pvi->FSName,
   //        MY_MX_PATH );

   HANDLE hFile = CreateFile(file, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
   BY_HANDLE_FILE_INFORMATION bhfi;
   memset( &bhfi, 0, sizeof(BY_HANDLE_FILE_INFORMATION) );
   GotAllocSize = FALSE;
   if( VFH(hFile) )
   {
       //GotAllocSize = GetFileInformationByHandleEx( hFile,
       //    FileIdBothDirectoryInfo, // FILE_ID_BOTH_DIR_INFO,
       //    (PVOID) &fibuf[0],
       //    MY_MX_BUF_SIZE );
       //if (GotAllocSize) {
       //    PFILE_ID_BOTH_DIR_INFO pi = (PFILE_ID_BOTH_DIR_INFO)&fibuf[0];
       //    if (pi->AllocationSize.QuadPart) {
       //        printf("%I64u", pi->AllocationSize.QuadPart);
       //    }
       //} else {
           // FILE_STANDARD_INFO
           GotAllocSize = GetFileInformationByHandleEx( hFile,
               FileStandardInfo, // FILE_STANDARD_INFO
               (PVOID) &fibuf[0],
               MY_MX_BUF_SIZE );
           //if (GotAllocSize) {
           //    PFILE_STANDARD_INFO psi = (PFILE_STANDARD_INFO)&fibuf[0];
           //    if (psi->AllocationSize.QuadPart) {
           //        printf("%I64u", psi->AllocationSize.QuadPart);
           //    }
           //}
       //}
       //GotVolInfo = GetVolumeInformationByHandleW( hFile,
      if( GetFileInformationByHandle(hFile, &bhfi) )
      {
         if( bhfi.dwFileAttributes & FILE_ATTRIBUTE_SPARSE_FILE )
            iret = 1;
      }
      CloseHandle(hFile);
   }
   return iret;
}

__int64 win_get_sparse_file_size( _TCHAR * file_name )
{
   LARGE_INTEGER li;
   __int64 i64;
   li.LowPart = GetCompressedFileSize( file_name, // LPCTSTR lpFileName,
      (LPDWORD)&li.HighPart );  // LPDWORD lpFileSizeHigh
   i64 = li.QuadPart;
   if( li.LowPart == INVALID_FILE_SIZE )
   {
      /* potential error */
      if( GetLastError() != NO_ERROR )
         return -1;
   }
   return i64;
}

char * get_time_string( __int64 time )
{
   char * pb = (char *)get_next_buffer();
   struct tm * ptm;
   ptm = utc_option ? gmtime (&time) : localtime (&time);
   sprintf (pb, "%04ld-%02d-%02d %02d:%02d:%02d %s",
		   ptm->tm_year + 1900L, ptm->tm_mon + 1, ptm->tm_mday,
		   ptm->tm_hour, ptm->tm_min, ptm->tm_sec,
         ( utc_option ? "(utc)" : "(local)" ) );
   return pb;
}

void out_time_stg( struct __stat64 * pbuf )
{
   printf( "mtime: %s", get_time_string( pbuf->st_mtime ) );
   if( tim_option ) {
      if ( pbuf->st_atime != pbuf->st_mtime )
         printf( " atime: %s", get_time_string( pbuf->st_atime ) );
      if ( pbuf->st_ctime != pbuf->st_mtime )
         printf( " ctime: %s", get_time_string( pbuf->st_ctime ) );
   }
}

void show_stat32( _TCHAR * pt, __int64 s64 )
{
   struct stat st;
   char * cp = (char *)pt;
   Convert_2_ASCII( pt );
   if( stat( cp, &st ) == 0 ) {
      __int64 i64 = st.st_size;
      if( i64 != s64 )
         printf( "   32-bit stat shows %ld bytes ...\n", st.st_size );
   }
}

// just return the next static buffer
// ==================================
#define  BUF_SIZE    264
#define  NUM_OF_BUFS  16
static int next_buffer_num = 0;
static _TCHAR _s_buffer[ BUF_SIZE * NUM_OF_BUFS ];
_TCHAR * get_next_buffer( void )
{
   next_buffer_num++;
   if (next_buffer_num >= NUM_OF_BUFS)
      next_buffer_num = 0;
   return &_s_buffer[ BUF_SIZE * next_buffer_num ];
}
// ====================================
void pre_scan_for_verb( int argc, _TCHAR* argv[] )
{
   int i;
   _TCHAR * arg, *sarg;
   _TCHAR c;
   for ( i = 1; i < argc; i++ )
   {
      arg = argv[i];
      if(*arg == '-') {
          sarg = &arg[1];
          while(*sarg == '-')
              sarg++;
         c = tolower(*sarg);
         if( c == 'v' ) {
            verb_on++;
            sarg++;
            while (*sarg) {
                if (tolower(*sarg) == 'v')
                    verb_on++;
                sarg++;
            }
         }
      }
   }
}

/* -----------------------------------------
struct _stat64 {
        _dev_t     st_dev;
        _ino_t     st_ino;
        unsigned short st_mode;
        short      st_nlink;
        short      st_uid;
        short      st_gid;
        _dev_t     st_rdev;
        __int64    st_size;
        __time64_t st_atime;
        __time64_t st_mtime;
        __time64_t st_ctime;
        };
   ----------------------------------------- */

void do_stat(_TCHAR *sarg)
{
    struct __stat64 buf;
    int sp = 0;
    if( STAT( sarg, &buf ) == 0 ) {
        _TCHAR * pt = get_next_buffer();
        STRCPY(pt, sarg);
        if ( buf.st_mode & _S_IFDIR ) {
            PRINTF( _T("%d: %s is a DIRECTORY "), filcount, sarg );
            out_time_stg( &buf );
            PRINTF( _T("\n") );
        } else {
            // assume a FILE
            if( raw_option ) {
                PRINTF( _T("%I64d"), buf.st_size );
                PRINTF( _T("\n") );
            } else {
                __int64 i64;
                sp = win_is_sparse_file( sarg );
                PRINTF( _T("%d: %s %s bytes (%s) "), filcount,
                    sarg, get_nice_number(buf.st_size,10),
                    get_k_num( buf.st_size, 0, 0 ) );
               if (GotAllocSize) {
                   PFILE_STANDARD_INFO psi = (PFILE_STANDARD_INFO)&fibuf[0];
                   if (psi->AllocationSize.QuadPart) {
                       i64 = psi->AllocationSize.QuadPart;
                       PRINTF( _T("(du=%s) "), get_k_num( i64, 0, 0 ));
                    }
                }
                out_time_stg( &buf );
                if (( sp == 0 ) && verb_on )
                    printf( " (ns)" );
                PRINTF( _T("\n") );
                if ( sp > 0 ) {
                    i64 = win_get_sparse_file_size( sarg );
                    if (i64 != -1) {
                        PRINTF( _T("   Is sparse file of data size %s (%s)\n"),
                            get_nice_number( i64, 0 ), get_k_num( i64, 0, 0 ) );
                    } else {
                        PRINTF( _T("   Is sparse file, but failed to get size!)\n") );
                    }
                }
                show_stat32( pt, buf.st_size );
            }
        }
    }
    else
    {
        _tperror( _T("ERROR") );
        PRINTF( _T("Unable to stat %s!\n"), sarg );
    }
}

// main entry point
int _tmain(int argc, _TCHAR* argv[])
{
    int i;
    _TCHAR *arg, *sarg;
    int c, cont;
    if( argc < 2 ) {
        give_help();
        return 1;
    }
    cont = 1;
    filcount = 0;
    pre_scan_for_verb( argc, argv );
    for ( i = 1; i < argc; i++ )
    {
        arg = argv[i];
        sarg = arg;
        c = *arg;
        cont = 1;
        if(VERB5) {
            PRINTF(_T("CMD %d: %s\n"), i, arg);
        }
        if (c == '-') {
            sarg = &arg[1];
            while(*sarg == '-')
               sarg++;
            c = tolower(*arg);
            switch(c)
            {
            case '?':
            case 'h':
                give_help();
                return 0;
            case 'u':
                utc_option = 1;
                break;
            case 'l':
                utc_option = 0;
                break;
            case 'f':
                sarg++;
                c = *sarg;
                if( c == '=' )
                {
                    sarg++;
                    cont = 0;   // fall through to do STAT
                }
                else
                    goto Bad_Param;
                break;
            case 's':
                raw_option = 1;
                break;
            case 't':
                tim_option = 1;
                break;
            case 'v':
                // done verb_on++;
                break;
            default:
Bad_Param:
                PRINTF(_T("ERROR: Unknown command %s ... Aborting \n"), arg );
                return 1;
            }
            if( cont > 0 ) {
                continue;
            }
        }
        filcount++;
        do_stat(sarg);
    }
	return 0;
}

// eof - stat64.cpp
