
// stat64.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.  
// #define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#define _WIN32_WINNT 0x0600 // for some additional functions
#endif						

#include <sys\types.h>
#include <sys\stat.h>
#include <windows.h> // includes <WinNls.h>  // for CP_UTF8
#include <stdio.h>
#include <tchar.h>
#include <string.h>
#include <time.h>    // for gmtime()

// eof - stat64.h
