@setlocal

@set DOINSTALL=0

@set TMPDIR=F:
@REM set TMPRT=%TMPDIR%\FG\18
@set TMPRT=..
@set TMPVER=1
@set TMPPRJ=stat64
@set TMPSRC=%TMPRT%
@set TMPBGN=%TIME%
@set TMPINS=C:/MDOS
@set TMPCM=%TMPSRC%\CMakeLists.txt
@set SET_BAT=%ProgramFiles(x86)%\Microsoft Visual Studio 10.0\VC\vcvarsall.bat
@if NOT EXIST "%SET_BAT%" goto NOBAT

@REM set DOPAUSE=pause
@set DOPAUSE=ask
@set DOPINST=0
@if "%~1x" == "NOPAUSEx" (
@set DOPINST=1
@shift
)

@call chkmsvc %TMPPRJ% 

@if EXIST build-cmake.bat (
@call build-cmake
)

@if NOT EXIST %TMPCM% goto NOCM

@if /I "%PROCESSOR_ARCHITECTURE%" EQU "AMD64" (
@set TMPINST=%TMPROOT%\software.x64
) ELSE (
 @if /I "%PROCESSOR_ARCHITECTURE%" EQU "x86_64" (
@set TMPINST=%TMPROOT%\software.x64
 ) ELSE (
@echo.
@echo ERROR: Appears 64-bit is NOT available... aborting...
@echo Maybe PROCESSOR_ARCHITECTURE=%PROCESSOR_ARCHITECTURE% not AMD64 or x86_64! If so *** FIX ME ***
@echo.
@goto ISERR
 )
)

@set TMPLOG=bldlog-1.txt
@set TMPOPTS=
@REM set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINS%
:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Doing build output to %TMPLOG%
@echo Doing build output to %TMPLOG% > %TMPLOG%

@set BLDDIR=%CD%

@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%'
@echo Doing: 'call "%SET_BAT%" %PROCESSOR_ARCHITECTURE%' >> %TMPLOG%
@call "%SET_BAT%" %PROCESSOR_ARCHITECTURE% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR0

@REM call setupqt64
@cd %BLDDIR%

@set TMPOPTS=%TMPOPTS% -G "Visual Studio 10 Win64"

@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%

@echo Doing 'cmake %TMPSRC% %TMPOPTS%'
@echo Doing 'cmake %TMPSRC% %TMPOPTS%' >> %TMPLOG%
@cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

@echo Doing 'cmake --build . --config Debug'
@echo Doing 'cmake --build . --config Debug' >> %TMPLOG%
@cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing: 'cmake --build . --config Release'
@echo Doing: 'cmake --build . --config Release' >> %TMPLOG%
@cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3
:DONEREL

@REM fa4 "***" %TMPLOG%
@echo.
@call elapsed %TMPBGN%
@echo.
@echo Appears a successful build... see %TMPLOG%
@echo.

@if "%DOINSTALL%x" == "0x" (
@echo.
@echo Skipping install for now... change me 'set DOINSTALL=1'...
@echo.
@goto END
)

@if "%DOPINST%x" == "1x" (
@echo No pause requested...
@goto DOINST
)

@%DOPAUSE% Continue with Release install to %TMPINS%? Only 'y' continues...
@if ERRORLEVEL 2 goto NOASK
@if ERRORLEVEL 1 goto DOINST
@echo.
@echo No install at this time...
@echo.
@goto END

:NOASK
@echo.
@echo Warning: Utility ask not found in PATH! SKIPPING INSTALL
@echo.
@goto END

:DOINST

@goto DNDBGINST
cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1
:DNDBGINST

cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1

@fa4 " -- " %TMPLOG%
@echo.

@call elapsed %TMPBGN%
@echo.
@echo All done build and install ... see %TMPLOG%
@echo.

@goto END

:NOCM
@echo Error: Can NOT locate %TMPCM%
@goto ISERR

:ERR0
@echo MSVC 10 setup error
@goto ISERR

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR

:ERR3
@fa4 "mt.exe : general error c101008d:" %TMPLOG% >nul
@if ERRORLEVEL 1 goto ERR33
:ERR34
@echo ERROR: Cmake build Release FAILED!
@goto ISERR
:ERR33
@echo Try again due to this STUPID STUPID STUPID error
@echo Try again due to this STUPID STUPID STUPID error >>%TMPLOG%
cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR34
@goto DONEREL

:NOBAT
@echo Can NOT locate MSVC setup batch "%SET_BAT%"! *** FIX ME ***
@goto ISERR

:ISERR
@echo See %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
