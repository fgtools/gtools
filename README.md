# GTools Project

README.md - 20160329 - 20150806 - 20140822 - 20140216

This is just a set of tools, developed over the years, put here in case they can be useful to other.

Some are **WINDOWS ONLY**, very Microsoft Windows specific, but others are of a moe generic nature where some have been ported to linux.

Most here are under a GNU GPL version 2 licence. Read the LICENCE.txt. Except ask, which is BSD 3 Clause License

Each sub-directory is a different, standalone product.

Some of these product may have their own licence, which is 
intended to prevail for that product.

### Project List:

#### Probably Windows Only

 - showlnk - Attempt to parse and dump a Windows shortcut (*.lnk) found in the Desktop - WIP - probably Windows ONLY  
 - xdelete - A **POWERFUL** delete folder utility. **CARE WHEN USING** Can delete important files - Windows ONLY  
 - stat64 - some windows experiments with stat 32/64 and UNICODE in Window ONLY
 
#### Maybe Crossported

 - json-checker - Simple app to check a json file. Original source from : http://www.json.org/JSON_checker/  
 - ask - simple utility to output a prompt and wait for keyboard input. if 'y' then exit 0, else exit 1  

Enjoy...

Geoff R. McLane  
issues: https://gitlab.com/fgtools/gtools/issues  
email: reports _at_ geoffair _dot_ info

; eof
