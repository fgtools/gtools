@setlocal
@set TMPEXE=Debug\xdeleted.exe
@set TMPINP=testinp.txt
@if NOT EXIST %TMPEXE% goto ERR1
@if NOT EXIST %TMPINP% goto ERR2

%TMPEXE% @%TMPINP% > tempout.txt
np tempout.txt

@goto END

:ERR1
@echo Error: Can NOT locate the EXE %TMPEXE%! Has it been built?
@goto END

:ERR2
@echo Error: Can NOT locate the INPUT file %TMPINP%! *** FIX ME ***
@goto END

:END


