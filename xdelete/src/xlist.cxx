
// xlist.c - list handling ...

#include "xdelete.h"

PMYLIST  g_pPATHS = 0;
PMYLIST  g_pFILES = 0;
PMYLIST g_TEMP_Skipped = 0;
PMYLIST g_TEMP_Deleted = 0;

void kill_a_list( PMYLIST * phead )
{
   if(phead)
   {
      PMYLIST pn = *phead;
      PMYLIST head;
      while(pn) {
         head = pn;     // will FREE this entry
         pn = pn->pnext; // get next
         free(head); // free last
      }
      *phead = 0;
   }
}

void kill_lists( void )
{
   kill_a_list( &g_pPATHS );
   kill_a_list( &g_pFILES );
#ifdef  MULTI_DIRECTORIES
   kill_a_list( &g_pINDIR );
#endif   // #ifdef  MULTI_DIRECTORIES
   kill_a_list( &g_TEMP_Skipped );
   kill_a_list( &g_TEMP_Deleted );
}

#ifdef _MSC_VER
// dwFileAttributes 
// File attributes of the file found.
// This member can be one or more of the following values. Attribute Meaning 
typedef struct tagFILEATTS {
   DWORD att;
   TCHAR * desc;
   TCHAR * sdesc;
} FILEATTS, * PFILEATTS;

FILEATTS fileatts[] = {
   { FILE_ATTRIBUTE_ARCHIVE, "The file or directory is an archive file or directory. Applications use this attribute to mark files for backup or removal.", "A" },
   { FILE_ATTRIBUTE_COMPRESSED, "The file or directory is compressed. For a file, this means that all of the data in the file is compressed. For a directory, this means that compression is the default for newly created files and subdirectories.", "C" },
   { FILE_ATTRIBUTE_DIRECTORY, "The handle identifies a directory.", "D" },
   { FILE_ATTRIBUTE_ENCRYPTED, "The file or directory is encrypted. For a file, this means that all data in the file is encrypted. For a directory, this means that encryption is the default for newly created files and subdirectories.", "E" },
   { FILE_ATTRIBUTE_HIDDEN, "The file or directory is hidden. It is not included in an ordinary directory listing.", "H" },
   { FILE_ATTRIBUTE_NORMAL, "The file or directory has no other attributes set. This attribute is valid only if used alone.", "N" },
   { FILE_ATTRIBUTE_OFFLINE, "The file data is not immediately available. This attribute indicates that the file data has been physically moved to offline storage. This attribute is used by Remote Storage, the hierarchical storage management software. Applications should not arbitrarily change this attribute.", "O" },
   { FILE_ATTRIBUTE_READONLY, "The file or directory is read-only. Applications can read the file but cannot write to it or delete it. In the case of a directory, applications cannot delete it.", "R" },
   { FILE_ATTRIBUTE_REPARSE_POINT, "The file or directory has an associated reparse point.", "P" },
   { FILE_ATTRIBUTE_SPARSE_FILE, "The file is a sparse file.", "F" },
   { FILE_ATTRIBUTE_SYSTEM, "The file or directory is part of the operating system or is used exclusively by the operating system.", "S" },
   { FILE_ATTRIBUTE_TEMPORARY, "The file is being used for temporary storage. File systems attempt to keep all of the data in memory for quicker access, rather than flushing it back to mass storage. A temporary file should be deleted by the application as soon as it is no longer needed.", "T" },
   { 0, 0, 0 } // final entry
};
void Append_Attributes( PTSTR ps, PMYLIST pn )
{
   PFILEATTS pfa = &fileatts[0];
   while( pfa->sdesc ) {
      if( pfa->att == pn->attrib )
         strcat( ps, pfa->sdesc );
      else if( pn->attrib & pfa->att )
         strcat( ps, pfa->sdesc );
      pfa++;
   }
}

#endif   // _MSC_VER

void get_file_stg( PTSTR ps, PMYLIST pn )
{
   size_t len;
#ifdef USE_64BIT_COUNTERS
   ULARGE_INTEGER size64 = pn->ul_size64;
#else
   unsigned long size = pn->ul_size;
#endif
   strcpy(ps, pn->name);
   while( strlen(ps) < MX_FILE_NM )
      strcat(ps," ");
   if( pn->isdir ) {
      strcat( ps, " <DIR>" );
   } else {
      len = 9;
#ifdef USE_64BIT_COUNTERS
      if( size64.QuadPart < 10 )
         len = 9;
      else if( size64.QuadPart < 100 )
         len = 8;
      else if( size64.QuadPart < 1000 )
         len = 7;
      else if( size64.QuadPart < 10000 )
         len = 6;
      else if( size64.QuadPart < 100000 )
         len = 5;
      else if( size64.QuadPart < 1000000 )
         len = 4;
      else if( size64.QuadPart < 10000000 )
         len = 3;
      else if( size64.QuadPart < 100000000 )
         len = 2;
      else
         len = 1;
      while( len-- )
         strcat(ps," ");
      sprintf(EndBuf(ps), " %llu", size64.QuadPart);
#else
      if( size < 10 )
         len = 9;
      else if( size < 100 )
         len = 8;
      else if( size < 1000 )
         len = 7;
      else if( size < 10000 )
         len = 6;
      else if( size < 100000 )
         len = 5;
      else if( size < 1000000 )
         len = 4;
      else if( size < 10000000 )
         len = 3;
      else if( size < 100000000 )
         len = 2;
      else
         len = 1;
      while( len-- )
         strcat(ps," ");
      sprintf(EndBuf(ps), " %lu", size);
#endif
      while( strlen(ps) < MX_FILE_SZ )
         strcat(ps," ");
      sprintf(EndBuf(ps), " %s", ctime( &pn->time ) );
      len = (long)strlen(ps);
      while( len-- ) {
         if( ps[len] > ' ' )
            break;
         ps[len] = 0;
      }
#ifdef _MSC_VER
      //  tdir->td_curr.de_att = tdir->td_data.dwFileAttributes; // attributes of found item
      if(VERB9) {
         strcat( ps, " " );
         Append_Attributes( ps, pn );
      }
#endif // _MSC_VER
   }
}

TCHAR * get_k_num( unsigned long byts )
{
   static TCHAR _s_kbuf[264];
   TCHAR * pb = _s_kbuf;
   if( byts < 1024 ) {
      sprintf(pb, "%lu bytes", byts);
   } else if( byts < 1024*1024 ) {
      double ks = ((double)byts / 1024.0);
      sprintf(pb, "%0.2f KB", ks);
   } else if( byts < 1024*1024*1024 ) {
      double ms = ((double)byts / (1024.0*1024.0));
      sprintf(pb, "%0.2f MB", ms);
   } else {
      double gs = ((double)byts / (1024.0*1024.0*1024.0));
      sprintf(pb, "%0.2f GB", gs);
   }
   return pb;
}

TCHAR * get_k_num64( ULARGE_INTEGER uli )
{
   static TCHAR _s_kbuf64[264];
   TCHAR * pb = _s_kbuf64;
   double byts = ((double)uli.HighPart * 4294967296.0);
   byts += uli.LowPart;
   if( byts < 1024 ) {
      sprintf(pb, "%f bytes", byts);
   } else if( byts < 1024*1024 ) {
      double ks = ((double)byts / 1024.0);
      sprintf(pb, "%0.2f KB", ks);
   } else if( byts < 1024*1024*1024 ) {
      double ms = ((double)byts / (1024.0*1024.0));
      sprintf(pb, "%0.2f MB", ms);
   } else {
      double gs = ((double)byts / (1024.0*1024.0*1024.0));
      sprintf(pb, "%0.2f GB", gs);
   }
   return pb;
}

void show_lists( void )
{
   static TCHAR _s_buf1[1024];
   PTSTR    ps = _s_buf1;
   PMYLIST  pn;
   int fcnt = get_list_count(g_pFILES);
   int dcnt = get_list_count(g_pPATHS);
   int tdcnt = get_list_count(g_TEMP_Deleted);
   int tscnt = get_list_count(g_TEMP_Skipped);

#ifdef USE_64BIT_COUNTERS
   ULARGE_INTEGER ul;
   ULARGE_INTEGER byttot64 = { 0, 0 };
   ULARGE_INTEGER dirtot64 = { 0, 0 };
#else
   unsigned long byttot = 0;
   unsigned long dirtot = 0;
#endif
#ifdef USE_64BIT_COUNTERS
   ULARGE_INTEGER tstot64 = { 0, 0 };
   ULARGE_INTEGER tdtot64 = { 0, 0 };
#else
   unsigned long tstot = 0;
   unsigned long tdtot = 0;
#endif
   unsigned long dirtot = 0;
   if( fcnt ) {
      if( g_do_delete[0] ) {
         sprtf( "Will delete %d files, as follows ..."MEOR, fcnt );
      } else {
         sprtf( "WOULD delete %d files if -d optione added, as follows ..."MEOR, fcnt );
      }
      if( g_do_clean[0] ) {
          if (VERB9 && tscnt) {
              // show TEMP skipped names
              sprtf("Pay special attention to the %d 'temp' files skipped..."MEOR, tscnt);
              for( pn = g_TEMP_Skipped; pn != 0; pn = pn->pnext ) {
#ifdef USE_64BIT_COUNTERS
                    tstot64.QuadPart += pn->ul_size64.QuadPart;
#else
                    tstot += pn->ul_size;
#endif
                    get_file_stg( ps, pn );
                    strcat(ps,MEOR);
                    sprtf(ps);
                }
#ifdef USE_64BIT_COUNTERS
               sprtf( "Skipped %d 'temp' files, approx. %s ..."MEOR, tscnt, get_k_num64(tstot64) );
#else
               sprtf( "Skipped %d 'temp' files, approx. %s ..."MEOR, tscnt, get_k_num(tstot) );
#endif
          }
          if (VERB5 && tdcnt) {
              // show TEMP deleted names
              sprtf("Pay attention to the %d 'temp' files to be deleted..."MEOR, tdcnt);
              for( pn = g_TEMP_Deleted; pn != 0; pn = pn->pnext ) {
#ifdef USE_64BIT_COUNTERS
                    tdtot64.QuadPart += pn->ul_size64.QuadPart;
#else
                    tdtot += pn->ul_size;
#endif
                    get_file_stg( ps, pn );
                    strcat(ps,MEOR);
                    sprtf(ps);
                }
#ifdef USE_64BIT_COUNTERS
               sprtf( "Will delete %d 'temp' files, approx. %s ..."MEOR, tdcnt, get_k_num64(tdtot64) );
#else
               sprtf( "Will delete %d 'temp' files, approx. %s ..."MEOR, tdcnt, get_k_num(tdtot) );
#endif
          }
      }
   } else {
      sprtf( "No files found to delete ..."MEOR );
   }

   for( pn = g_pFILES; pn != 0; pn = pn->pnext ) {
#ifdef USE_64BIT_COUNTERS
      byttot64.QuadPart += pn->ul_size64.QuadPart;
#else
      byttot += pn->ul_size;
#endif
      get_file_stg( ps, pn );
      strcat(ps,MEOR);
      sprtf(ps);
   }
   if( !g_do_clean[0] ) {
      if( dcnt ) {
         if( g_do_delete[0] && g_del_folders[0] ) {
            sprtf( "Will delete %d directories, as follows ..."MEOR, dcnt );
         } else {
            sprtf( "WOULD delete %d directories, if -%s option added, as follows ..."MEOR,
               dcnt,
               ( g_do_delete[0] ? "f" : "df" ) );
         }
      } else {
         sprtf( "No directories found to delete ..."MEOR );
      }
      for( pn = g_pPATHS; pn != 0; pn = pn->pnext ) {
         dirtot += ASSUME_FOLDER_SIZE;
         get_file_stg( ps, pn );
         strcat(ps,MEOR);
         sprtf(ps);
      }
   } else {
      sprtf( "-c CLEAN option presently does NOT delete folders ..."MEOR );
      //******** add more output - more info if -v2 *******
      //if( VERB2 ) {  // FIX20090305 - add more clean ouput
         show_clean_counts();
      //}
   }

#ifdef USE_64BIT_COUNTERS
   ul.QuadPart = byttot64.QuadPart + dirtot64.QuadPart;
   sprtf( "Summary: %d dirs, %d files, approx. %s ..."MEOR, dcnt, fcnt,
      get_k_num64(ul) );
#else
   sprtf( "Summary: %d dirs, %d files, approx. %s ..."MEOR, dcnt, fcnt,
      get_k_num(byttot + dirtot) );
#endif
}

PMYLIST get_list_last( PMYLIST head )
{
   PMYLIST  pn;
   if( head ) {
      pn = head->pnext;
      while(pn) {
         head = pn; // keep last
         pn = pn->pnext;
      }
   }
   return head;
}

int get_list_count( PMYLIST head )
{
   int   icnt = 0;
   PMYLIST  pn;
   if( head ) {
      icnt++;
      pn = head->pnext;
      while(pn) {
         icnt++;
         pn = pn->pnext;
      }
   }
   return icnt;
}

PMYLIST  get_list_item( PMYLIST head, int item )
{
   int   icnt = 0;
   PMYLIST  pn;
   if( head ) {
      if( icnt == item ) {
         return head;
      }
      icnt++;
      pn = head->pnext;
      while(pn) {
         if( icnt == item ) {
            return pn;
         }
         icnt++;
         pn = pn->pnext;
      }
   }
   return NULL;   // failed to get a list item
}

PMYLIST get_list_before( PMYLIST head, PMYLIST nxt )
{
   PMYLIST  pn;
   if( head == nxt )
      return NULL;
   if( head ) {
      pn = head->pnext;
      if( pn == nxt )
         return head;

      while(pn) {
         head = pn; // keep last
         pn = pn->pnext;
         if( pn == nxt )
            return head;
      }
   }
   return NULL;
}

void  add_2_end( PMYLIST head, PMYLIST nxt, char * lname )
{
   PMYLIST  pn;
   int   i = 0;
   if( head ) {
      pn = head->pnext;
      i = 1;
      while(pn) {
         head = pn; // keep last
         pn = pn->pnext;
         i++;
      }
      if(VERB9) {
         sprtf("Added [%s] as the LAST item in %s (%d)."MEOR,
         nxt->name,
         (lname ? lname : "unknown"),
         (i+1) );
      }
      head->pnext = nxt;
   }
}

#ifndef _MSC_VER
#define ZeroMemory(a,b) memset(a,0,b)
#endif

PMYLIST  add_2_list( PMYLIST * pList, char * path, char * lname )
{
   PMYLIST head = *pList;
   size_t   siz = sizeof(MYLIST) + strlen(path);
   PMYLIST nxt = (PMYLIST)malloc( siz );
   CHKMEM(nxt);
   ZeroMemory( nxt, siz );
   //nxt->done = 0;
   //nxt->pnext = 0;
   strcpy(nxt->name, path);
   if( head ) {
      add_2_end( head, nxt, lname );
   } else {
      *pList = nxt;
      if(VERB9) {
         sprtf("Added [%s] as the FIRST item in %s (1)."MEOR, path,
         (lname ? lname : "unknown") );
      }
   }
   return nxt; // return the structure ...
}

PMYLIST  add_2_list_if_new( PMYLIST * pList, char * path, char * lname )
{
   PMYLIST head = *pList;
   PMYLIST  pn = 0;
   traverse_my_list( head, pn )
   {
      if( pn ) {
         if( strcmp( pn->name, path ) == 0 ) {
            return pn;
         }
      } else
         break;
   }
   return( add_2_list( pList, path, lname ) );
}

// FIX20070815 - check if in LIST
PMYLIST  is_in_list( PMYLIST * pList, char * path, char * lname )
{
   PMYLIST head = *pList;
   PMYLIST  pn = 0;
   traverse_my_list( head, pn )
   {
      if( pn ) {
         if( strcmp( pn->name, path ) == 0 ) {
            return pn;
         }
      } else
         break;
   }
   return NULL;
}


PMYLIST  add_2_paths( char * path )
{
   return( add_2_list( &g_pPATHS, path, "PATHS" ) );
}
PMYLIST  add_2_files( char * path )
{
   return( add_2_list( &g_pFILES, path, "FILES" ) );
}

PMYLIST  add_2_TEMP_Skipped( char * path )
{
   return( add_2_list( &g_TEMP_Skipped, path, "SKIPPED" ) );
}

PMYLIST  add_2_TEMP_Deleted( char * path )
{
   return( add_2_list( &g_TEMP_Deleted, path, "DELETED" ) );
}


// eof - xlist.c
