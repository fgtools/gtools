
// xclean.c - perform a MSVC clean up, like clobnow.bat

#include "xdelete.h"
#include "xfnmatch.h"

//typedef struct tagCLEAN {
//   TCHAR * clean_mask2;
//   DWORD clean_counter;
//}CLEAN, * PCLEAN;

// { "temp*", "*", 0 },
CLEAN clean_masks_OLD_NOT_USED[] = {
{ ".old", 0 },
{ ".bak", 0 },
{ ".obj", 0 },
{ ".err", 0 },
{ ".pdb", 0 },
{ ".lst", 0 },
{ ".pch", 0 },
{ ".ilk", 0 },
{ ".ncb", 0 },
{ ".plg", 0 },
{ ".opt", 0 },
{ ".idb", 0 },
{ ".aps", 0 },
{ ".sbr", 0 },
{ ".suo", 0 }, // add this HIDDEN file
{ 0 , 0 }
};

CLEAN clean_masks2[] = {
{ "*.old", 0 },
{ "*.bak", 0 },
{ "*.obj", 0 },
{ "*.err", 0 },
{ "*.pdb", 0 },
{ "*.lst", 0 },
{ "*.pch", 0 },
{ "*.ilk", 0 },
{ "*.ncb", 0 },
{ "*.plg", 0 },
{ "*.opt", 0 },
{ "*.idb", 0 },
{ "*.aps", 0 },
{ "*.sbr", 0 },
{ "*.suo", 0 }, // add this HIDDEN file
// FIX20080531 - add some MORE to CLEAN -cd[r] option - does NOT delete folders
{ "*.manifest", 0 },
{ "*.res", 0 },
{ "*.user", 0 },
// FIX20100522 - add *.bsc
{ "*.bsc", 0 },
{ "mt.dep", 0 },
{ "BuildLog.htm", 0 },
// FIX20120415 - add *.sdf, *.ipch - also added .NUM.tlog files...
{ "*.sdf", 0 },
{ "*.ipch", 0 },
{ 0 , 0 }
};

CLEAN clean_temp = { "temp", 0 };
CLEAN clean_tlog = { "tlog", 0 };

#define  X_NAME_MAX  264
static TCHAR _s_cmpbuf[8 * X_NAME_MAX];

static char _s_temp[MY_NAME_MAX];

void add_to_temp_skipped( PTSTR pfil, PTSTR pdir, MyDirEnt *de )
{
    PTSTR nxt = _s_temp;
    PMYLIST pn;
    strcpy(nxt, pdir);
    strcat(nxt, MY_SLASH);
    strcat(nxt, pfil);
    pn = add_2_TEMP_Skipped(nxt);
    if (pn) {
        pn->isdir = 0;
        pn->isclean = 2;
#ifdef USE_64BIT_COUNTERS
        pn->ul_size64.QuadPart = de->de_size64.QuadPart;
#else
        pn->ul_size = de->de_size;
#endif
        pn->time = de->de_time;
#ifdef   _MSC_VER
        pn->attrib = de->de_att;
#endif   // _MSC_VER
    }
}
void add_to_temp_deleted( PTSTR pfil, PTSTR pdir, MyDirEnt *de )
{
    PTSTR nxt = _s_temp;
    PMYLIST pn;
    strcpy(nxt, pdir);
    strcat(nxt, MY_SLASH);
    strcat(nxt, pfil);
    pn = add_2_TEMP_Deleted(nxt);
    if (pn) {
        pn->isdir = 0;
        pn->isclean = 1;
#ifdef USE_64BIT_COUNTERS
        pn->ul_size64.QuadPart = de->de_size64.QuadPart;
#else
        pn->ul_size = de->de_size;
#endif
        pn->time = de->de_time;
#ifdef   _MSC_VER
        pn->attrib = de->de_att;
#endif   // _MSC_VER
    }
}


BOOL  BeginwithTEMP( PTSTR pfil, PTSTR pdir, MyDirEnt *de )
{
   size_t len = strlen(pfil);
   if( len >= 4 ) {
      if( ( toupper(pfil[0]) == 'T' ) &&
          ( toupper(pfil[1]) == 'E' ) &&
          ( toupper(pfil[2]) == 'M' ) &&
          ( toupper(pfil[3]) == 'P' ) )
      {
          
          if ( (len == 4) || ( ( _strnicmp( pfil, "template", 8 ) ) && ( _strnicmp( pfil, "temperat", 8 ) ) && ( _strnicmp( pfil, "temporar", 8 ) ) ) ) {
          // if( strcmpi( pfil, "Template" ) ) { // let this one through!!!
            clean_temp.clean_counter++;
            if (VERB5) add_to_temp_deleted( pfil, pdir, de );
            return TRUE;
         } else if (VERB9) {
             add_to_temp_skipped( pfil, pdir, de );
          }
      }
   }
   return FALSE;
}

// FIX20070815 - add to show 'internal' msvc -c clean item list - VERB5
void Add_Clean_Exts( PTSTR pb )
{
   PCLEAN pc = &clean_masks2[0]; // FIX20080723 - if clean (-cd[r]) add ALL
   int   cnt = 0;
   while( pc->clean_mask2 ) {
      if(cnt) {
         strcat( pb, ", " );
      }
      strcat( pb, pc->clean_mask2 );
      pc++;
      cnt++;
   }
   strcat( pb, ", and temp*.*, except 'template'" );
}

BOOL  IsCleanExt( PTSTR pext )
{
   PCLEAN pc = &clean_masks2[0]; // FIX20080723 - if clean (-cd[r]) add ALL
   while( pc->clean_mask2 ) {
      if( strcmpi( pext, pc->clean_mask2 ) == 0 ) {
         pc->clean_counter++;
         return TRUE;
      }
      pc++;
   }
   return FALSE;
}

// FIX20120415 - Delete MSVC ?????.num.tlog
BOOL is_numbered_tlog( PTSTR pf )
{
    int res = InStri( pf, ".tlog" );
    if (res) {
        size_t len = strlen(pf);
        size_t i, d;
        PTSTR a1;
        for (i = 0; i < len; i++) {
            a1 = &pf[i];
            if (*a1 == '.') {
                d = len - 1;
                if ((d >= 7) && (ISNUM(pf[i+1])) ) {
                    PTSTR bgn = &pf[i+1];
                    i++;
                    for (; i < len; i++) {
                        a1 = &pf[i];
                        if (*a1 == '.') {
                            // we MAY have a .NUM.tlog...
                            i++;
                            a1 = &pf[i];
                            if (strcmp(a1,"tlog") == 0) {
                                clean_tlog.clean_counter++;
                                return TRUE;
                            }
                        } else if (!ISNUM(pf[i]))
                            break;
                    }
                }
            }
        }
    }
    return FALSE;
}

#ifndef _MSC_VER
// spoof of _splitpath for use in unix
void _splitpath( const char *fullpath, char *drive, char *dir, char *fname, char *ext )
{
    size_t ii, len = strlen(fullpath);
    if (drive)
        *drive = 0;
    if (dir)
        *dir = 0;
    if (fname)
        *fname = 0;
    if (ext)
        *ext = 0;
    if (!len)
        return;
    char *ofp = strdup(fullpath);
    char *pfp = ofp;
    int c, had_dot, had_sep;
    if ((len >= 2) && (pfp[1] == ':')) {
        if (drive) {
            drive[0] = *pfp++;
            drive[1] = *pfp++;
            drive[2] = 0;
        } else
            pfp += 2;
    }
    
    len = strlen(pfp);
    if (!len) {
        free(ofp);
        return;
    }
    had_dot = 0;
    had_sep = 0;
    for (ii = len - 1; ii > 0; ii--) {
        c = pfp[ii];
        if ( !had_dot && (c == '.')) {
            had_dot = 1;
            if (ext) {
                strcpy(ext,&pfp[ii]);
            }
            pfp[ii] = 0;    // drop the extension
        } else if ( !had_sep && ( ( c == '/' ) || ( c == '\\' ) ) ) {
            had_sep = 1;
            if (fname) {
                strcpy(fname,&pfp[ii+1]);
            }
            pfp[ii+1] = 0; // drop the file name
            break;
        }
    }
    if (dir) {
        strcpy(dir,pfp);
    }
    free(ofp);
}
#endif // !_MSC_VER

BOOL  IsInCleanList2( PTSTR pf, PTSTR pdir, MyDirEnt *de )
{
   PTSTR nf = _s_cmpbuf; // MemAlloc( 8 * X_NAME_MAX );
   PTSTR drv1 = &nf[0*X_NAME_MAX];
   PTSTR pth1 = &nf[1*X_NAME_MAX];
   PTSTR fil1 = &nf[2*X_NAME_MAX];
   PTSTR ext1 = &nf[3*X_NAME_MAX];
   *drv1 = 0; /* mainly for debug view */
   *pth1 = 0;
   *fil1 = 0;
   *ext1 = 0;
   _splitpath( pf, drv1, pth1, fil1, ext1 );
   if( BeginwithTEMP( fil1, pdir, de ) ) {
      return TRUE;
   } else if( (strlen(ext1) == 4) && IsCleanExt( ext1 ) ) {
      return TRUE;
   } else if (is_numbered_tlog(pf)) {
       return TRUE;
   }

   return FALSE;
}

BOOL  IsInCleanList( PTSTR pf, PTSTR pdir, MyDirEnt *de )
{
   PCLEAN pc = &clean_masks2[0];
   while( pc->clean_mask2 ) {
      if( fnmatch (pc->clean_mask2, pf, FNM_NOESCAPE | FNM_CASEFOLD ) == 0 ) {
         pc->clean_counter++;
         if(VERB9) {
            sprtf( "Matched [%s], with [%s]"MEOR,
               pc->clean_mask2,
               pf );
         }
         return TRUE;
      }
      pc++;
   }
   if( IsInCleanList2( pf, pdir, de ) ) {
      if(VERB9) {
         sprtf( "Matched [%s] in IsInCleanList2 (but NOT fnmatch???)"MEOR,
            pf );
      }
       return TRUE;
   }
   return FALSE;
}

// FIX20090305 - add more clean ouput
void show_clean_counts( void )
{
   PCLEAN pc = &clean_masks2[0];
   int   notzcnt = 0;
   unsigned long total = 0;
   while( pc->clean_mask2 ) {
      if( pc->clean_counter )
         notzcnt++;
      total += pc->clean_counter;
      pc++;
   }
   total += clean_temp.clean_counter;
   if( clean_temp.clean_counter )
      notzcnt++;

   pc = &clean_masks2[0];
   sprtf("clean %u: ", total );
   while( pc->clean_mask2 ) {
      if( pc->clean_counter ) {
         sprtf( "%s=%u ", pc->clean_mask2, pc->clean_counter );
      }
      pc++;
   }

   // FIX20120415 - Also clean the .NUM.tlog genrated by MSVC10
   // Should maybe also clean the <project>.log file, but that is a little dangerous ;=()
   if( clean_tlog.clean_counter )
      sprtf( "%s=%u ", clean_tlog.clean_mask2, clean_tlog.clean_counter );

   if( clean_temp.clean_counter )
      sprtf( "%s=%u", clean_temp.clean_mask2, clean_temp.clean_counter );
   sprtf("\n");
}

// eof - xclean.c
