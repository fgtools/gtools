/* 
    xdelete.cxx - the main() OS entry

    GNU GPL Version 2

    Copyright (C) Geoff R. McLane - 2006 - 2014

 */
#include "xdelete.h"
//#include <conio.h>
extern BOOL please_NO_confirmation; // FIX20080906 added "-NO-confirmation-please"
extern int default_wait;  // ms to to wait

#define  MY_CYC_COUNT   5
// variable set and used by program
PTSTR g_pProg = 0;
#ifdef  MULTI_DIRECTORIES
PMYLIST  g_pINDIR = 0;
TCHAR * g_pCurDir = 0;
int   g_incount = 0;
#else // !#ifdef  MULTI_DIRECTORIES
TCHAR * g_dir = 0;
#endif   // #ifdef  MULTI_DIRECTORIES y/n

// files
unsigned long g_file_count = 0;
#ifdef USE_64BIT_COUNTERS
ULARGE_INTEGER g_total64_size = { 0 };
#else
unsigned long g_total_size = 0;
#endif
// folders
unsigned long g_dir_count = 0;
unsigned long g_total_dsize = 0;

unsigned long g_file_clean = 0;
unsigned long g_failed_files = 0;
unsigned long g_failed_dirs = 0;
unsigned long g_modified_files = 0;
unsigned long g_modified_dirs = 0;

// DELETION COUNTS
unsigned long g_files_delete = 0;
unsigned long g_files_failed = 0;
unsigned long long g_files_btotal = 0;
unsigned long g_dir_delete = 0;
unsigned long g_dir_failed = 0;
unsigned long long g_dirs_btotal = 0;

// #define  MX_WORK_BUF    1024+16
// #define  MX_WORK_OVR    MX_WORK_BUF - 256 // stop BEFORE END

TCHAR G_work_buf[MX_WORK_BUF];   // [1024] TEXT BUFFER
#define  CHKBUF(a)   if(strlen(a) > MX_WORK_OVR) {                             \
               sprtf( "ERROR: INTERNAL: Internal buffer too small!"MEOR );     \
               sprtf( "Recompile - set MX_WORK_OVR &gt; %d!\n", MX_WORK_OVR ); \
               pgm_exit(PEXIT_ERROR);                                                   \
            }

void pgm_exit( int val )
{
   kill_lists();
#ifndef  MULTI_DIRECTORIES
   if(g_dir) free(g_dir); // done with strdup();
#endif   // #ifndef  MULTI_DIRECTORIES
   if( !((val == PEXIT_SUCCESS)||(val == PEXIT_PROBLEM)||(val == PEXIT_ERROR)) )
      val = PEXIT_ERROR; // always a 2 = error exit, if otherwise

   exit(val); // zero is good exit
}

int get_keyboard_character( void );

void  do_confirm( int max_cycs )
{
   int   chr = 0;
   int   cycs = 0;
   if( please_NO_confirmation ) // FIX20080906 added "-NO-confirmation-please"
   {
      sprtf( "Continue with DELETE? Waiting for %d seconds : ",
         (default_wait / 1000));  // seconds to to wait
      chr = toupper(get_keyboard_character());
      if(chr == 'Y')
      {
         sprtf( "Yes - Did not get otherwise!"MEOR );
         return;
      }
      sprtf( "Exiting program ..."MEOR );
      pgm_exit(PEXIT_PROBLEM);
   }

   if(VERB5) sprtf( "Continue with DELETE? (Y)es or (N)o only."MEOR );
   while( ( cycs < max_cycs ) && ( chr != 'Y' ) && ( chr != 'N' ) ) {
      printf( "Continue with DELETE? (Y)es or (N)o only : " );
      chr = toupper(get_keyboard_character());
      fflush(stdin);
      if( chr == 'Y' ) {
         printf( "Yes"MEOR );
         if(VERB5) sprtf( "GOT Yes"MEOR );
      } else if( chr == 'N' ) {
         printf( "No"MEOR );
         if(VERB5) sprtf( "GOT No"MEOR );
      } else {
         printf( "Only Y or N allowed!"MEOR );
         //sprtf( "Only Y or N allowed!"MEOR );
      }
      cycs++;
   }
   if( chr != 'Y' ) {
      printf( "Exiting program ..."MEOR );
      pgm_exit(PEXIT_PROBLEM);
   }
}

void delete_files( int tobin )
{
   PTSTR    ps = get_tmp_buf2(); // _s_buf2;
   PMYLIST  pn;
   long  ncnt, fcnt;
   if( VERB5 ) {
      sprtf( "Deleting files ...\n" );
   }
   ncnt = fcnt = 0;
   for( pn = g_pFILES; pn != 0; pn = pn->pnext ) {
      ncnt++;
      get_file_stg( ps, pn );
      if(VERB5) sprtf(ps); // show file name
      if( MyFileDelete( pn->name, tobin ) ) {
         if(VERB9) sprtf( " done"MEOR );
         // KEEP total bytes deleted
#ifdef USE_64BIT_COUNTERS
         g_files_btotal = pn->ul_size64.QuadPart;
#else
         g_files_btotal = pn->ul_size;
#endif
      } else {
         fcnt++;
         g_failed_files++;
         if(VERB1) {
            if( !VERB5 ) sprtf(ps); // out the FILE NAME
            sprtf( " FAILED! (%d)"MEOR, g_last_delete_error );
#ifdef  WIN32
            if(VERB1) {
               VOID * pMsgBuf = 0;
               FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                  NULL,
                  g_last_delete_error,	// Results of GetLastError()
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                  (LPTSTR) &pMsgBuf,
                  0,
                  NULL );
               if( pMsgBuf ) {
                  // show the string.
                  sprtf( "%s"MEOR, pMsgBuf );
                  LocalFree( pMsgBuf );
               }
            }
#endif // #ifdef  WIN32
         }
      }
   }
   if( VERB2 ) {
      if( ncnt ) {
         sprtf( "All done ... %d files ...\n", ncnt );
         if( fcnt ) {
            sprtf( "NOTE %d FAILED!\n", fcnt );
         }
      } else {
         sprtf( "No files to delete ...\n" );
      }
   } else if (VERB1 && (fcnt > 0)) {
      sprtf( "NOTE %d FAILED!\n", fcnt );
   }
   g_files_delete += ncnt - fcnt;
   g_files_failed += fcnt;
}

int   delete_paths( int tobin )
{
   static TCHAR _s_buf2[1024];
   PTSTR    ps = _s_buf2;
   PMYLIST pn;
   long  ncnt, fcnt;
   ncnt = fcnt = 0;
   pn = get_list_last( g_pPATHS );
   if( VERB2 ) {
      sprtf( "Deleting folders ...\n" );
   }
   if( pn ) {
      do {
         ncnt++;
         get_file_stg( ps, pn );
         if(VERB2)
            sprtf(ps);
         if( MyFileDelete( pn->name, tobin ) ) {
            if(VERB9) sprtf( " done"MEOR );
            // KEEP total bytes deleted
#ifdef USE_64BIT_COUNTERS
            g_dirs_btotal = pn->ul_size64.QuadPart;
#else
            g_dirs_btotal = pn->ul_size;
#endif
         } else {
            fcnt++;
            g_failed_dirs++;
            if(VERB2) {
               sprtf( " FAILED! (%d)"MEOR, g_last_delete_error );
            } else if(VERB1) {
               sprtf( "%s FAILED! (%d)"MEOR, ps, g_last_delete_error );
            }
            if(VERB1) {
#ifdef  WIN32
               VOID * pMsgBuf = 0;
               FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                  NULL,
                  g_last_delete_error,	// Results of GetLastError()
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                  (LPTSTR) &pMsgBuf,
                  0,
                  NULL );
               if( pMsgBuf ) {
                  // show the string.
                  sprtf( "%s"MEOR, pMsgBuf );
                  LocalFree( pMsgBuf );
               }
#endif // #ifdef  WIN32
            }
         }
      } while(( pn = get_list_before( g_pPATHS, pn )) != NULL );
   }
   if( VERB2 ) {
      if( ncnt ) {
         sprtf( "All done ... %d folders ...\n", ncnt );
         if( fcnt ) {
            sprtf( "NOTE %d FAILED!\n", fcnt );
         }
      } else {
         sprtf( "No folders to delete ...\n" );
      }
   } else if( VERB1 && (fcnt > 0) ) {
      sprtf( "NOTE %d FAILED!\n", fcnt );
   }
   g_dir_delete += ncnt - fcnt;
   g_dir_failed += fcnt;
   return ncnt;
}

void process_dir( char * dir )
{
   PMYLIST pn;
   MyDirEnt * de;
   MyDir * pdir = MyOpenDir(dir);
   char * nxt = (char *)malloc( MY_NAME_MAX );
   DWORD locfcnt = 0;
   DWORD locfdel = 0;
   CHKMEM(nxt);
   if( !pdir ) {
      sprtf("ERROR: Failed to open directory %s ...\n", dir );
      pgm_exit(PEXIT_ERROR);
   }
   if( VERB5 ) {
      sprtf( "Processing [%s] ..."MEOR, dir );
   }
   de = MyReadDir( pdir );
   while(de) {
      if( de->de_isdir ) {
         if( strcmp(de->de_name,".") && strcmp(de->de_name,"..") ) {
            strcpy(nxt, dir);
            strcat(nxt, MY_SLASH);
            strcat(nxt, de->de_name);
            pn = add_2_paths(nxt);
            pn->isdir = 1;
#ifdef   _MSC_VER
            pn->attrib = de->de_att;
#endif   // _MSC_VER
            g_dir_count++;
            g_total_dsize += ASSUME_FOLDER_SIZE;
         }
      } else {
         g_file_count++;
         locfcnt++;
         if( g_do_clean[0] ) {
            if( IsInCleanList( de->de_name, dir, de ) ) {
               strcpy(nxt, dir);
               strcat(nxt, MY_SLASH);
               strcat(nxt, de->de_name);
               pn = add_2_files(nxt);
               pn->isdir = 0;
               pn->isclean = 1;
#ifdef USE_64BIT_COUNTERS
               pn->ul_size64.QuadPart = de->de_size64.QuadPart;
#else
               pn->ul_size = de->de_size;
#endif
               pn->time = de->de_time;
#ifdef   _MSC_VER
               pn->attrib = de->de_att;
#endif   // _MSC_VER
#ifdef USE_64BIT_COUNTERS
               g_total64_size.QuadPart += de->de_size64.QuadPart;
#else
               g_total_size += de->de_size;
#endif
               locfdel++;
               g_file_clean++;
            }
         } else {
            // NOT due to CLEAN
            strcpy(nxt, dir);
            strcat(nxt, MY_SLASH);
            strcat(nxt, de->de_name);
            pn = add_2_files(nxt);
            pn->isdir = 0;
            pn->isclean = 0;
#ifdef USE_64BIT_COUNTERS
            pn->ul_size64.QuadPart = de->de_size64.QuadPart;
#else
            pn->ul_size = de->de_size;
#endif
            pn->time = de->de_time;
#ifdef   _MSC_VER
            pn->attrib = de->de_att;
#endif   // _MSC_VER
#ifdef USE_64BIT_COUNTERS
            g_total64_size.QuadPart += de->de_size64.QuadPart;
#else
            g_total_size += de->de_size;
#endif
         }
      }
      de = MyReadDir( pdir );
   }
   MyCloseDir( pdir );
   free(nxt);
}

int process_paths( int rec )
{
   int icnt = 0;
   PMYLIST head = g_pPATHS;
   PMYLIST  pn;
   for( pn = head; pn != 0; pn = pn->pnext ) {
      if( !pn->done ) {
         pn->done = 1;
         icnt++;
         if(rec)
            process_dir( pn->name );
      }
   }
   return icnt;
}

DWORD SectorsPerCluster, BytesPerSector, NumberOfFreeClusters, TotalNumberOfClusters;
ULARGE_INTEGER FreeBytesAvailable, TotalNumberOfBytes, TotalNumberOfFreeBytes;

//DWORD bSectorsPerCluster, bBytesPerSector, bNumberOfFreeClusters, bTotalNumberOfClusters;
//ULARGE_INTEGER bFreeBytesAvailable, bTotalNumberOfBytes, bTotalNumberOfFreeBytes;
//int      bshwn;

//BOOL GetDiskFreeSpace(
//  LPCTSTR lpRootPathName,
//  LPDWORD lpSectorsPerCluster,
//  LPDWORD lpBytesPerSector,
//  LPDWORD lpNumberOfFreeClusters,
//  LPDWORD lpTotalNumberOfClusters
//);
//BOOL GetDiskFreeSpaceEx(
//  LPCTSTR lpDirectoryName,
//  PULARGE_INTEGER lpFreeBytesAvailable,
//  PULARGE_INTEGER lpTotalNumberOfBytes,
//  PULARGE_INTEGER lpTotalNumberOfFreeBytes
//);
DWORDLONG DoMult3( DWORD dw1, DWORD dw2, DWORD dw3 )
{
	DWORDLONG	dwlr, dwl1, dwl2, dwl3;
	dwl1 = dw1;
	dwl2 = dw2;
	dwl3 = dw3;
	dwlr = dwl1 * dwl2;
	dwlr = dwlr * dwl3;
	return( dwlr );
}

void show_disk_space( void )
{
#if   (defined(WIN32) && defined(KEEP_DRIVE_LIST))
   PMYLIST  head = drive_list;
   PMYLIST  pn;
   PTSTR    ps;
   int      shwn;
   ULARGE_INTEGER uldiff;
   traverse_my_list( head, pn )
   {
      ps = pn->name;
      shwn = 0;
      if( GetDiskFreeSpaceEx( ps, //  LPCTSTR lpDirectoryName,
         &FreeBytesAvailable,
         &TotalNumberOfBytes,
         &TotalNumberOfFreeBytes ) )
      {
         PTSTR pb2 = get_tmp_buf2();
         PTSTR pb3 = get_tmp_buf3();
         sprintf(pb2, "%I64u", TotalNumberOfBytes);
         nice_num(pb3, pb2);
         sprtf( "Total disk %s is ", ps );
         sprtf( "%s (%s), ",
            get_k_num64( TotalNumberOfBytes ),
            pb3);

         sprintf(pb2, "%I64u", TotalNumberOfFreeBytes);
         nice_num(pb3, pb2);
         sprtf( "free %s (%s) bytes."MEOR,
            get_k_num64( TotalNumberOfFreeBytes ),
            pb3 );
         shwn |= 1;
         if( pn->bshwn == 1 ) {
            uldiff.QuadPart = TotalNumberOfFreeBytes.QuadPart - pn->bTotalNumberOfFreeBytes.QuadPart;
            if( uldiff.QuadPart > 0 ) {
               sprintf(pb2, "%I64u", uldiff);
               nice_num(pb3, pb2);
               sprtf( "Deletion gained %s (%s) bytes."MEOR,
                  get_k_num64( uldiff ),
                  pb3 );
            }
         }
      }

      if( GetDiskFreeSpace( ps, &SectorsPerCluster, &BytesPerSector,
         &NumberOfFreeClusters, &TotalNumberOfClusters) )
      {
         if( !shwn ) { // || VERB9 ) {
            DWORDLONG dwl1 = DoMult3( BytesPerSector, SectorsPerCluster, NumberOfFreeClusters );
            DWORDLONG dwl2 = DoMult3( BytesPerSector, SectorsPerCluster, TotalNumberOfClusters );
            sprtf( "Total disk %s is %lu bytes, free %lu bytes."MEOR,
               ps, dwl2, dwl1 );
            shwn |= 2;
            if( pn->bshwn == 2 ) {
               DWORDLONG diff;
               DWORDLONG dwl3 = DoMult3( pn->bBytesPerSector, pn->bSectorsPerCluster,
                  pn->bNumberOfFreeClusters );
               diff = dwl2 - dwl3;
               if( diff > 0 ) {
                  sprtf("Deletion gained %lu bytes."MEOR, diff );
               }
            }
         }
      }

      if( !shwn ) {
         if( VERB5 ) {
            sprtf("Unable to get totals for drive %s?"MEOR);
         }
      }
   }
#else
    sprtf("Deletion gained %llu bytes. %llu files, %llu dirs..."MEOR, 
        (g_files_btotal + g_dirs_btotal), g_files_btotal, g_dirs_btotal );
#endif   // #if   (defined(WIN32) && defined(KEEP_DRIVE_LIST))
}

void get_disk_space( void )
{
#if   (defined(WIN32) && defined(KEEP_DRIVE_LIST))
   PMYLIST  head = drive_list;
   PMYLIST  pn;
   PTSTR    ps;
   traverse_my_list( head, pn )
   {
      ps = pn->name;
      pn->bshwn = 0;
      if( GetDiskFreeSpaceEx( ps, //  LPCTSTR lpDirectoryName,
         &pn->bFreeBytesAvailable,
         &pn->bTotalNumberOfBytes,
         &pn->bTotalNumberOfFreeBytes ) )
      {
         //sprtf( "Total disk %s is ", ps );
         //sprtf( "%s (%I64u), ",
         //   get_k_num64( TotalNumberOfBytes ),
         //   TotalNumberOfBytes);
         //sprtf( "free %s (%I64u) bytes."MEOR,
         //   get_k_num64( TotalNumberOfFreeBytes ),
         //   TotalNumberOfFreeBytes );
         pn->bshwn = 1;
      }
      if( GetDiskFreeSpace( ps, &pn->bSectorsPerCluster, &pn->bBytesPerSector,
         &pn->bNumberOfFreeClusters, &pn->bTotalNumberOfClusters) )
      {
         if( !pn->bshwn ) { // || VERB9 ) {
         //   DWORDLONG dwl1 = DoMult3( BytesPerSector, SectorsPerCluster, NumberOfFreeClusters );
         //   DWORDLONG dwl2 = DoMult3( BytesPerSector, SectorsPerCluster, TotalNumberOfClusters );
         //   sprtf( "Total disk %s is %lu bytes, free %lu bytes."MEOR,
         //      ps, dwl1, dwl2 );
            pn->bshwn = 2;
         }
      }
   }
#endif   // #if   (defined(WIN32) && defined(KEEP_DRIVE_LIST))
}

void add_root_dir( TCHAR * dir )
{  // add this root
   PMYLIST pn = add_2_paths(dir);
   pn->done  = 1;
   pn->isdir = 1;
#ifdef   _MSC_VER
   pn->attrib = GetFileAttributes( dir );
#endif // _MSC_VER
   g_dir_count++; // bump FOLDER count
   g_total_dsize += ASSUME_FOLDER_SIZE;
   process_dir( dir ); // process the ROOT folder, which may add more
}

#ifdef  MULTI_DIRECTORIES
void  process_in_list( void )
{
   TCHAR * pb = get_tmp_buf3();  // _s_buf3;
   int   i;
   PMYLIST pn;

   *pb = 0; // start buffer
   // output this information if VERB9
   for( i = 0; i < g_incount; i++ ) {
      pn = get_list_item( g_pINDIR, i );
      if(pn) {
         g_pCurDir = pn->name;
         if( MyDirExists( g_pCurDir ) ) {
            if(*pb) {
               strcat(pb,MEOR);
            }
            strcat(pb,g_pCurDir);
            CHKBUF(pb);
         } else {
            sprtf( "ERROR: %s does not appear to be a DIRECTORY ...\n", g_pCurDir );
            pgm_exit(PEXIT_ERROR);
         }
      } else {
         sprtf( "ERROR: INTERNAL: Function failed to return pointer!\n" );
         pgm_exit(PEXIT_ERROR);
      }
      if(VERB9) sprtf("%s"MEOR, pb);

      *pb = 0; // reset buffer
   }
   if(VERB5) {
      if( g_incount > 1 ) {
         sprtf( "For the folders -"MEOR );
         for( i = 0; i < g_incount; i++ ) {
            pn = get_list_item( g_pINDIR, i );
            sprtf( "%s"MEOR, pn->name );
         }
      } else {
         sprtf( "For the folder [%s]"MEOR, g_pCurDir );
      }
   }
   *pb = 0;
   // FIX20070815 - only if VERB2 use long options,
   // else use new SHORT option list
   if( VERB2 ) {
      strcpy(pb,"Options: ");
      get_option_list( pb );
   } else {
      strcpy(pb,"Opts: ");
      get_short_option_list( pb );
   }
   sprtf( "%s"MEOR, pb );

   if( g_do_clean[0] && VERB5 ) {   // // FIX20070815
      strcpy( pb, "Cleaning: " );
      Add_Clean_Exts( pb );
      sprtf( "%s list."MEOR, pb );
   }

   *pb = 0; // start buffer
   for( i = 0; i < g_incount; i++ )
   {
      pn = get_list_item( g_pINDIR, i );
      if(pn) {
         g_pCurDir = pn->name;
         if( MyDirExists( g_pCurDir ) ) {
            add_root_dir( g_pCurDir );
            if(*pb) {
               strcat(pb,MEOR);
            }
            strcat(pb,g_pCurDir);
            CHKBUF(pb);
            if(VERB5) sprtf("%s"MEOR, pb );
            *pb = 0; // restart buffer
         } else {
            sprtf( "ERROR: %s does not appear to be a DIRECTORY ...\n", g_pCurDir );
            pgm_exit(PEXIT_ERROR);
         }
      } else {
         sprtf( "ERROR: INTERNAL: Function failed to return pointer!\n" );
         pgm_exit(PEXIT_ERROR);
      }
   }
   if( strlen(pb) ) {
      sprtf("%s"MEOR, pb);
   }

   *pb = 0;
   process_paths( g_do_recursive[0] ); // now process these ...

}

#endif // #ifdef  MULTI_DIRECTORIES

int   save_verb;
void show_totals( void )
{
    if(g_do_clean[0]) // FIX20090305 - add more clean ouput
        show_clean_counts();
#ifdef USE_64BIT_COUNTERS
    sprtf( "Got %d dirs, %d files, total (approx) %s (%llu bytes) ...\n",
        g_dir_count,
        (g_do_clean[0] ? g_file_clean : g_file_count),
        get_k_num64( g_total64_size ),
        g_total64_size );
#else
    sprtf( "Got %d dirs, %d files, total (approx) %s (%u bytes) ...\n",
        g_dir_count,
        (g_do_clean[0] ? g_file_clean : g_file_count),
        get_k_num( g_total_size ),
        g_total_size );
#endif
}

int main( int argc, char * * argv )
{
   int   doneclean = 0; // FIX20070815 - only output DISK is delete done, or VERB2
   g_pProg = argv[0];

   pgm_init(); // any initialization required, like is REDIRECTION on ...

#ifndef  MULTI_DIRECTORIES
   if( argc < 2 ) {
      sprtf( "ERROR: Must input a directory to delete ...\n" );
      show_help(argv[0]);
      pgm_exit(PEXIT_ERROR);
   }
#endif // #ifndef  MULTI_DIRECTORIES

   process_args( argc, argv );

#ifdef  MULTI_DIRECTORIES
//extern PMYLIST  g_pINDIR;
//extern TCHAR * g_pCurDir;
   if( g_do_frontpage[0] ) // -p delete frontpage folders
   {
      add_frontpage_dirs();
   }

   g_incount = get_list_count( g_pINDIR );
   //if( g_pINDIR == 0 ) {
   if( g_incount == 0 ) {
      sprtf( "ERROR: No directory input found ... \n" );
      pgm_exit(PEXIT_ERROR);
      // add_2_list( &g_pINDIR, "." );
      // g_incount = get_list_count( g_pINDIR );
   }
   process_in_list();

#else // !#ifdef  MULTI_DIRECTORIES
//extern TCHAR * g_dir = 0;
   if( g_dir == 0 ) {
      sprtf( "ERROR: Must input a directory to delete ...\n" );
      show_help(argv[0]);
      pgm_exit(PEXIT_ERROR);
   } else if( !MyDirExists( g_dir ) ) {
      sprtf( "ERROR: %s does not appear to be a DIRECTORY ...\n", g_dir );
      pgm_exit(PEXIT_ERROR);
   }

   add_root_dir( g_dir );

   // process_dir( g_dir ); // process the ROOT folder, which may add more

   process_paths( g_do_recursive[0] ); // now process these ...

   sprtf( "For the folder [%s] ... %s %s\n", g_dir,
      (g_do_recursive[0] ? "recursive" : "only"),
      (g_do_clean[0] ? "CLEAN" : ""));
#endif   // #ifdef  MULTI_DIRECTORIES y/n

   show_totals();

   if( VERB2 ) {
      show_lists();
   }

   get_disk_space(); // get disk space BEFORE any DELETIONS

   if( g_do_delete[0] ) {
      /* *************************************************************
         WE ARE IN THE DELETE FILES AND DIRECTORIES ZONE
         ************************************************************* */
      int fcnt = get_list_count(g_pFILES);
      int dcnt = get_list_count(g_pPATHS);
      int tdcnt = get_list_count(g_TEMP_Deleted);
      int tscnt = get_list_count(g_TEMP_Skipped);
      if( g_do_clean[0] && ( fcnt == 0 ) ) { // FIX20070815
         // FIX20070815 - do NOT confirm this ZERO
         sprtf( "No (-c) files to 'clean' ...\n" );
      } else {
         do_confirm( MY_CYC_COUNT );
         if( fcnt ) {
            sprtf( "NOTE WELL: File List %d will be deleted ... ARE YOU SURE?\n", fcnt );
            if( !g_do_clean[0] && g_del_folders[0] && dcnt ) {
               sprtf( "NOTE WELL: Folder List %d will be deleted ... ARE YOU SURE?\n", dcnt );
            }
            show_totals();
            do_confirm( MY_CYC_COUNT );
            sprtf( "File List %d being deleted ...\n", fcnt );
            delete_files( g_tobin[0] );
            doneclean++;   // FIX20070815
         } else {
            sprtf( "NO files to delete ...\n" );
         }
         if( !g_do_clean[0] ) {
            if( g_del_folders[0] ) {
               if( dcnt ) {
                  sprtf( "Folder List %d being deleted ...\n", dcnt );
                  delete_paths( g_tobin[0] );
                  doneclean++;   // FIX20070815
               } else {
                  sprtf( "NO folders to delete ...\n" );
               }
            }
         } else {
            sprtf( "Clean option can not presently delete %d folders ...\n", dcnt );
         }

         // ALWAYS SHOW FAILURES
         // ++++++++++++++++++++
         save_verb = g_verbal[0];
         g_verbal[0] = 1;
         if(g_failed_files) {
            sprtf( "FAILED to delele %d files ...\n", g_failed_files );
         }
         if(g_failed_dirs) {
            sprtf( "FAILED to delele %d folders ...\n", g_failed_dirs );
         }
         if(g_failed_files || g_failed_dirs) {
            if( g_do_modify[0] ) {
               sprtf( "This failure is even with the -m (modify) option!\n" );
            } else {
               sprtf( "Deletion may be possible setting -m (modify) option!\n" );
            }
         }
         sprtf( "Deletions: " );
         if(( g_files_delete > 0 )||
            ( g_files_failed > 0 )||
            ( g_dir_delete   > 0 )||
            ( g_dir_failed   > 0 ))
         {
            if(( g_files_delete > 0 )||
               ( g_files_failed > 0 ))
            {
               sprtf(" Files %d", g_files_delete );
               if( g_files_failed > 0 )
                  sprtf( ", FAILED %d", g_files_failed );
            }
            if(( g_dir_delete > 0 )||
               ( g_dir_failed > 0 ))
            {
               sprtf(" Directories %d", g_dir_delete );
               if( g_dir_failed > 0 )
                  sprtf( ", FAILED %d", g_dir_failed );
            }
            sprtf(MEOR);
         }
         else
         {
            sprtf( "None listed"MEOR );
         }
         g_verbal[0] = save_verb;
      }
   } else {
      sprtf( "List not deleted due to no -d switch ...\n" );
   }

   if( doneclean || VERB2 )
      show_disk_space(); // get disk space AFTER any DELETIONS

   pgm_exit(PEXIT_SUCCESS);

   return 0;
}


// eof - xdelete.c
