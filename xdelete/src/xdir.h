
// xdir.h - directory handling ...
#ifndef __xdir_h__
#define __xdir_h__

#ifdef   WIN32
#include "xlist.h"
#endif // #ifdef   WIN32


#ifdef   _MSC_VER
#define  MY_SLASH  "\\"
#define  MY_IFDIR  _S_IFDIR
#else /* !_MSC_VER */
#define  MY_SLASH  "/"
#define  MY_IFDIR  S_IFDIR
#endif /* _MSC_VER y/n */
#define  MY_NAME_MAX 264

typedef struct _MyDir MyDir;
typedef struct tagMyDirEnt
{
  char de_name[ MY_NAME_MAX+1 ];
#ifdef USE_64BIT_COUNTERS
  ULARGE_INTEGER de_size64;
#else
  long de_size;
#endif
  time_t de_time;
  unsigned short de_mode;
  int de_isdir;
#ifdef _MSC_VER
   unsigned long de_att; // attributes of found item
#endif // _MSC_VER
}MyDirEnt;

struct _MyDir
{
  char td_dirname[MY_NAME_MAX+1]; /* current folder being scanned */
  MyDirEnt td_curr; /* current entry in folder */
#ifdef _MSC_VER
  char td_search[MY_NAME_MAX+1]; /* search name */
  WIN32_FIND_DATA td_data; /* search data */
  HANDLE td_hFind; /* find handle */
  int td_first;  /* set for first item */
  int td_done;   /* no more entries */
#else
  DIR * td_pdir;
#endif
};

extern int MyDirExists( char * dirname );
extern MyDir * MyOpenDir( char * dirnm );
extern MyDirEnt * MyReadDir( MyDir * tdir );
extern void MyCloseDir( MyDir * tdir );
extern int g_last_delete_error;
extern BOOL  MyFileDelete( PTSTR lpf, BOOL tobin );
#if   (defined(WIN32) && defined(KEEP_DRIVE_LIST))
extern PMYLIST drive_list;
#endif // #ifdef   WIN32

#endif // #ifndef __xdir_h__
// eof - xdir.h
