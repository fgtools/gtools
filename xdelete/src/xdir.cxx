
// xdir.c

#include "xdelete.h"
#include <errno.h>
#ifdef _MSC_VER
#define  xdel_stat   _stat
#else
#define  xdel_stat   stat
#include <unistd.h> // for unlink(), ...
#endif

#if   (defined(WIN32) && defined(KEEP_DRIVE_LIST))
TCHAR dir_drive[8];
PMYLIST drive_list = 0;
#endif // #ifdef   WIN32

struct xdel_stat g_stat_buf;
int MyDirExists( char * dirname )
{
   // check if entry is a directory
   if( xdel_stat( dirname, &g_stat_buf) == 0 ) {
      if( g_stat_buf.st_mode & MY_IFDIR ) {
#if   (defined(WIN32) && defined(KEEP_DRIVE_LIST))
         dir_drive[0] = (TCHAR) g_stat_buf.st_dev + 'A';
         dir_drive[1] = (TCHAR) ':';
         dir_drive[2] = (TCHAR) '\\';
         dir_drive[3] = 0;
         // FIX20070815 - reduce the VERB9 noise
         if( !is_in_list( &drive_list, dir_drive, "DRIVES" ) ) {
            if( VERB9 ) sprtf( "Adding [%s] to DRIVE LIST ..."MEOR, dir_drive );
            add_2_list_if_new( &drive_list, dir_drive, "DRIVES" );
         }
#endif // #ifdef   WIN32
         return 1;
      }
   }
   /* set g_stat_buf.st_atime; and g_stat_buf.st_size; */

   return 0;
}

MyDir * MyOpenDir( char * dirnm )
{
   MyDir * tdir = (MyDir *)malloc( sizeof(MyDir) );
   if (tdir != NULL )
   {
      strcpy( tdir->td_dirname, dirnm ) ;
#ifdef _MSC_VER
      {
         size_t len;
         strcpy(tdir->td_search, dirnm);
       
         /* remove any trailing directory separator */
         len = strlen(tdir->td_search);
         if( ( len > 0 ) && strchr( "/\\", tdir->td_search[len-1]) ) {
            /* remove trailing SLASH, of either type */
            tdir->td_search[len-1] = 0;
            /* from BOTH */
            tdir->td_dirname[len-1] = 0;
         }
         /* add the wild card for ALL */
         strcat( tdir->td_search, MY_SLASH"*.*" );

         tdir->td_first = 1; /* set FIRST available ... */
         tdir->td_done = 0;  /* not DONE yet ... */
         tdir->td_hFind = FindFirstFile( tdir->td_search, &tdir->td_data);
         if( tdir->td_hFind == INVALID_HANDLE_VALUE )
         {
            free(tdir); /* not directory */
            tdir = NULL;
         }
      }
#else
      tdir->td_pdir = opendir(dirnm);
      if( tdir->td_pdir == NULL )
      {
         free(tdir);
         tdir = NULL;
      }
#endif
   }
   return tdir;
}

MyDirEnt * MyReadDir( MyDir * tdir )
{
   static char _dir_path[1000 + MY_NAME_MAX];
   static struct stat _s_buf;

   // get next entry from the directory
#ifdef _MSC_VER
   // check state
   if( tdir->td_first ) {
      tdir->td_first = 0;
   } else if ( !tdir->td_done &&
      !FindNextFile( tdir->td_hFind, &tdir->td_data ) ) {
      tdir->td_done = 1;
   }

   if( tdir->td_done )
      return NULL;

   /* get current entry from structure */
   strcpy( tdir->td_curr.de_name, tdir->td_data.cFileName );
#else /* !_MSC_VER */
   struct dirent * pdirent = readdir( tdir->td_pdir );

   if( !pdirent )
      return NULL;

   strcpy( tdir->td_curr.de_name, pdirent->d_name );
#endif /* _MSC_VER y/n */

   /* build fully qualified path to current entry,
      of the native platform type ... */
   sprintf( _dir_path, "%s%s%s",
      tdir->td_dirname,
      MY_SLASH,
      tdir->td_curr.de_name );
   /* set if entry is a directory */
   tdir->td_curr.de_isdir = MyDirExists( _dir_path );
   /* fill in the details */
#ifdef USE_64BIT_COUNTERS
   tdir->td_curr.de_size64.QuadPart = g_stat_buf.st_size;
#else
   tdir->td_curr.de_size = g_stat_buf.st_size;
#endif
   tdir->td_curr.de_time = g_stat_buf.st_mtime;
   tdir->td_curr.de_mode = g_stat_buf.st_mode;
#ifdef _MSC_VER
   tdir->td_curr.de_att = tdir->td_data.dwFileAttributes; // attributes of found item
#endif // _MSC_VER
   if( VERB9 ) {
      if( strcmp(tdir->td_curr.de_name, ".") &&
          strcmp(tdir->td_curr.de_name, "..") )
      {
         sprtf( "%s %ld"MEOR, _dir_path, g_stat_buf.st_size );
      }
   }

   return( &tdir->td_curr );

}

void MyCloseDir( MyDir * tdir )
{
   if( tdir != NULL )
   {
#ifdef _MSC_VER
      if( tdir->td_hFind && ( tdir->td_hFind != INVALID_HANDLE_VALUE ) )
         FindClose( tdir->td_hFind );
      tdir->td_hFind = 0;
#else /* !_MSC_VER */
      if( tdir->td_pdir )
         closedir( tdir->td_pdir );
      tdir->td_pdir = NULL;
#endif
      free(tdir);
   }
}

// When used to delete a file, SHFileOperation(&fo) will attempt
// to place the deleted file in the Recycle Bin.
// If you wish to delete a file and guarantee that it will
// not be placed in the Recycle Bin, use DeleteFile.
// DeleteFile(pnp);
//typedef struct _SHFILEOPSTRUCT{ 
//    HWND hwnd; 
//    UINT wFunc; 
//    LPCTSTR pFrom; 
//    LPCTSTR pTo; 
//    FILEOP_FLAGS fFlags; 
//    BOOL fAnyOperationsAborted; 
//    LPVOID hNameMappings; 
//    LPCSTR lpszProgressTitle; 
//} SHFILEOPSTRUCT, *LPSHFILEOPSTRUCT;
int g_last_delete_error;

BOOL  DeleteAnEntry( PTSTR lpf, BOOL tobin )
{
   BOOL              bRet = FALSE;
   size_t            dwi  = strlen(lpf);
   if( !dwi ) {
      g_last_delete_error = ENOENT; // in errno.h
      return FALSE;
   }
#ifdef _MSC_VER
   static SHFILEOPSTRUCT    shfo;
   LPSHFILEOPSTRUCT  pfo = &shfo;
   g_last_delete_error = 0; // none, so far ;=))
   if( tobin ) {
      dwi++;
      lpf[dwi] = 0;  // An additional NULL character must be appended to the
      // end of the final name to indicate the end of pFrom.
      ZeroMemory( pfo, sizeof(SHFILEOPSTRUCT) );
      pfo->hwnd = NULL; // hwndClient;
      pfo->wFunc = FO_DELETE; // delete file in pFrom
      pfo->pFrom = lpf;       // pointer to file
      pfo->fFlags = FOF_ALLOWUNDO | FOF_SILENT | FOF_NOERRORUI | FOF_NOCONFIRMATION;
      if( SHFileOperation( pfo ) == 0 ) {
         bRet = TRUE;
      }
   } else {
      if( MyDirExists( lpf ) )
         bRet = RemoveDirectory( lpf );
      else
         bRet = DeleteFile( lpf );
   }
   if( !bRet ) {
      if( errno )
         g_last_delete_error = errno;
      else
         g_last_delete_error = GetLastError();
      if( !g_last_delete_error )
         g_last_delete_error = EACCES; // put in SOME error no-matter-what
   }
#else // _MSC_VER
     // printf("WARNING: port of DeleteAnEntry needs to be checked! '%s'\n",lpf);
     if( MyDirExists( lpf ) ) {
        if (rmdir(lpf) == 0) {
            bRet = TRUE;
        } else {
            g_last_delete_error = errno;
        }
     } else {
        if (unlink(lpf) == 0) {
            bRet = TRUE;
        } else {
            g_last_delete_error = errno;
        }
     }
#endif // #ifdef _MSC_VER
   return bRet;
}

// Change the file-permission settings.
//int _chmod( 
//   const char *filename,
//   int pmode 
//);
//Parameters
// filename - Name of existing file. 
// pmode - Permission setting for file. 
// Return Value
// These functions return 0 if the permission setting is successfully changed.
// A return value of �1 indicates that the specified file could not be found,
// in which case errno is set to ENOENT.
// Remarks
// The _chmod function changes the permission setting of the file specified by
// filename. The permission setting controls read and write access to the file.
// The integer expression pmode contains one or both of the following manifest
// constants, defined in SYS\STAT.H: 
// _S_IWRITE = Writing permitted. 
// _S_IREAD  = Reading permitted. 
// _S_IREAD | _S_IWRITE = Reading and writing permitted. 
// Any other values for pmode are ignored. When both constants are given,
// they are joined with the bitwise OR operator ( | ). If write permission is not given,
// the file is read-only. Note that all files are always readable; it is not possible
// to give write-only permission.
// Thus the modes _S_IWRITE and _S_IREAD | _S_IWRITE are equivalent.
BOOL  SetFileReadWrite( PTSTR pf )
{
#ifdef _MSC_VER
   int res = _chmod( pf, _S_IREAD | _S_IWRITE );
   if( res == 0 ) {
      return TRUE;
   }
#else // !_MSC_VER
    printf("WARNING: port of SetFileReadWrite needs to be checked!\n");
    int res = chmod( pf, S_IRWXU | S_IRWXG | S_IRWXO );
    if (res == 0 ) {
        return TRUE;
    }
#endif // _MSC_VER y/n

   return FALSE;
}

BOOL  MyFileDelete( PTSTR lpf, BOOL tobin )
{
   BOOL              bRet = FALSE;
   size_t            dwi  = strlen(lpf);
   if( !dwi ) {
      g_last_delete_error = ENOENT; // in errno.h
      return FALSE;
   }
   bRet = DeleteAnEntry( lpf, tobin );
   if( !bRet ) {
      if( g_do_modify[0] ) {
         // we have one more chance
         SetFileReadWrite( lpf );
         bRet = DeleteAnEntry( lpf, tobin );
      }
   }
   return bRet;
}

// eof - xdir.c
