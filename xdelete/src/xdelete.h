
// xdelete.h - included in everything ...
#ifndef __xdelete_h__
#define __xdelete_h__

#include "xvers.h"   // my version file
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>  /* may need <varargs.h> for Unix V */
#include <stdint.h>
#include <time.h>
#ifdef _MSC_VER
/////////////////////////////////////////////////////////////////
#include <windows.h>
#include <direct.h>
#include <io.h>   // for _chmod(char * fn, pmode) like _S_IREAD | _S_IWRITE
/////////////////////////////////////////////////////////////////
#else // !_MSC_VER
/////////////////////////////////////////////////////////////////
#include <string.h> // for strcpy(), ...
#include <ctype.h> // toupper(), ?
#include <strings.h> // strncasecmp(), ...
#include <dirent.h>
#define _strnicmp strncasecmp
#define strcmpi strcasecmp
#define stricmp strcasecmp
/////////////////////////////////////////////////////////////////
#endif // !_MSC_VER y/n

#ifndef _MSC_VER
/////////////////////////////////////////////////////////
typedef char * PTSTR;
typedef char * LPSTR;
typedef char CHAR;
typedef char TCHAR;
typedef int BOOL;
typedef uint32_t DWORD;
typedef uint64_t DWORDLONG;
typedef FILE *HANDLE;
typedef int * PINT;
typedef void * LPVOID;
#define INVALID_HANDLE_VALUE (HANDLE)-1

typedef struct _ULARGE_INTEGER {
    union {
        struct {
        uint32_t LowPart;
        uint32_t HighPart;
        };
        uint64_t QuadPart;
     };
} ULARGE_INTEGER;

typedef struct _FILETIME {
    uint32_t dwLowDateTime;
    uint32_t dwHighDateTime;
} FILETIME, *PFILETIME, *LPFILETIME;

typedef struct _SYSTEMTIME {
    int16_t wYear;
    int16_t wMonth;
    int16_t wDayOfWeek;
    int16_t wDay;
    int16_t wHour;
    int16_t wMinute;
    int16_t wSecond;
    int16_t wMilliseconds;
} SYSTEMTIME, *PSYSTEMTIME, *LPSYSTEMTIME;

    


#define TRUE 1
#define FALSE 0
/////////////////////////////////////////////////////////
#endif // !_MSC_VER

#include "xdir.h"
#include "xprt.h"
#include "xhelp.h"
#include "xlist.h"
#include "xclean.h"
#include "xfrontpage.h"
#include "xkey.h"
#include "xutil.h"

#define  CHKMEM(a) if(!a){ sprtf( "ERROR: Memory FAILED!\n" ); pgm_exit(PEXIT_ERROR); }

#define  ISNUM(a) (( a >= '0' ) && ( a <= '9' ))
#define  EndBuf(a)   ( a + strlen(a) )

extern int g_verbal[];
extern PTSTR g_pProg;

#define  VERB1    ( g_verbal[0] >= 1 )
#define  VERB2    ( g_verbal[0] >= 2 )
#define  VERB5    ( g_verbal[0] >= 5 )
#define  VERB9    ( g_verbal[0] >= 9 )

extern void pgm_exit( int val );
#ifdef  MULTI_DIRECTORIES
extern PMYLIST  g_pINDIR;
extern TCHAR * g_pCurDir;

#else // !#ifdef  MULTI_DIRECTORIES
extern TCHAR * g_dir = 0;
#endif   // #ifdef  MULTI_DIRECTORIES y/n

#endif // #ifndef __xdelete_h__
// eof - xdelete.h
