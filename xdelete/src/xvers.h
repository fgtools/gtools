// xvers.h
#ifndef _xvers_h
#define _xvers_h

// FIX20140218 - convert ot use a cmake build system
// also add it to my gtools repo to try a linux compile
#define VERS    "1.0.11"

// FIX20120415 - Fix 'template' pass through
// VERS    "1.0.10"

// FIX20100619 - switch to using 64-bit counters for total size
//  VERS  "1.0.9"  // FIX20100619 - use 64-bit counters 
#define USE_64BIT_COUNTERS

// FIX20100522 - add *.bsc ie { "*.bsc", 0 }, to clean_masks2 array
//#define  VERS  "1.0.8"  // FIX20100522 - add *.bsc, 
// and this version number matches the zip file number - see upd.bat

// no 6 or 7...

// FIX20090411 -dir=<directory> - alternate for enterning <directory>
// VERS  "1.0.5"  // FIX20090411 - added -dir=<directory>

// FIX20090305
// VERS  "1.0.4"  // FIX20090305 - add more clean ouput

// FIX20090112
// VERS  "1.0.3"  // FIX20090112 - add nn=secs to HELP output
// FIX20080906
// VERS  "1.0.2"  // FIX20080906 - add -please-NO-prompt[=nn] nn=secs,
// to avoid the confirmation also - only exits are -
#define  PEXIT_SUCCESS      0    // successful deletion, or HELP asked
#define  PEXIT_PROBLEM      1    // no confirmation 'Y'
#define  PEXIT_ERROR        2    // some error in command, memory, or other causing an abort
// and changed to all *.cxx files, and added nice number (with commas) to total disk

// FIX20080723
// VERS  "1.0.1"  // FIX20080723 - if clean (-cd[r]) add ALL per the following
// using all clean_masks2 throughout ...

// FIX20080531
//  VERS  "1.0.0"  // FIX20080531 - if clean (-cd[r]) add 
// "BuildLog.htm", "*.manifest", "*.res",  and "mt_dep"

// VERS  "0.9.8"  // FIX20070815 - if clean (-cd[r]) and NO files found,
// exit without stupid question.

//  VERS  "0.9.7"  // 2007/06/07 - remove static buffer overflow
// define a MAXIMUM work buffer size - 1024 should __ALWAYS__ be ok ;=))
// The rules of a MX_WORK_BUF, is that is only to be used as relatively
// short sentences for ouput, output, then cleared - this should really
// maximise any text output to about 256 bytes, output with line ending MEOL
// see // xprt.h - output handling ... sprtf(pb,...) to console, and LOG file.
#define  MX_WORK_BUF    1024+16  // add 16 overflow
#define  MX_WORK_OVR    MX_WORK_BUF - 256 // stop BEFORE END

// VERS  "0.9.6"  // 2007/06/04 - add special FrontPage folder DELETE
#define  KEEP_DRIVE_LIST   // show directory space
// VERS  "0.9.5"  // 2007/03/07 - add DOUBLE CONFIRM - annoying, but CHECK CAREFULLY!
// VERS  "0.9.4"  // 2006/11/25 - added Options output BEFORE search ...
// VERS  "0.9.3"  // 2006/07/20 - added -@input and multiple directories
#define  MULTI_DIRECTORIES

// VERS  "0.9.2"  // 2006/06/06 - added file and folder count to output info
// VERS  "0.9.1"  // 2006/04/09 - first release 

#ifdef _MSC_VER
#pragma warning(disable:4996) // 2006/11/01 - using MSVC8 - depreciation
#endif

#endif // #ifndef _xvers_h
// eof - xvers.h

