// xutil.h
#ifndef _xutil_h_
#define _xutil_h_

extern void nice_num( char * dst, char * src ); // get nice number, with commas
extern PTSTR get_tmp_buf( void );
extern PTSTR get_tmp_buf2( void );
extern PTSTR get_tmp_buf3( void );

// test if string s is anywhere in str - case insensitive
extern int InStri( char * str, char * s );

#endif // _xutil_h_
// xutil.h

