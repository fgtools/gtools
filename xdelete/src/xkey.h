// xkey.h
#ifndef _xkey_h_
#define _xkey_h_

#define  VFH(a)   ( a && ( a != INVALID_HANDLE_VALUE ) )

extern void pgm_init( void ); // any initialisation required

extern int default_wait;  // ms to to wait
extern HANDLE ghStdOut;
extern HANDLE ghErrOut;
extern BOOL  gbRedON;  // gotred - got redirection of stdout
extern BOOL  gbErrON;  // got redirection of stderr

#endif // _xkey_h_
// eof - xkey.h
