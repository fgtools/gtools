
// xprt.c
#include "xdelete.h"
TCHAR deffile[] = "templog.txt";

// #define  add_debug   printf
#define  add_debug

TCHAR log_file[264];
FILE * log_hand = 0;
void oi( PTSTR buf );

#ifndef _MSC_VER
DWORD GetModuleFileName( HANDLE h, PTSTR pname, DWORD len )
{
    printf("WARNING: GetModuleFileName needs to be ported\n");
    return 0;
}
#endif


void open_log_file( void )
{
   if( log_hand == 0 ) {
      size_t len;
      int c;
      PTSTR pl = log_file;
      *pl = 0;
      add_debug( "Opening log file ...\n" );
      if( GetModuleFileName( NULL, pl, 256 ) ) {
         add_debug( "Module name is %s ...\n", pl );
      } else {
         // failed ... try another way
         if(g_pProg) {
            strcpy(pl, g_pProg);
            add_debug( "Module name is %s ...\n", pl );
         }
      }
      len = strlen(pl);
      while( len > 0 ) {
         len--;
         c = pl[len];
         if(( c == '\\') || (c == '/')) {
            len++;
            break;
         }
         pl[len] = 0;
      }
      add_debug( "Path name is %s ...\n", pl );
      len = strlen(pl);
      if( ( len > 0 ) && ( strchr( "\\/", pl[len-1] ) == 0 ) ) {
         strcat(pl, "\\");
      }
      strcat(pl,deffile);
      add_debug( "Log name is %s ...\n", pl );

      log_hand = fopen(pl, "wb"); /* = "templog.txt"; */
      if( !log_hand ) {
         add_debug( "Open log %s FAILED! ...\n", pl );
         log_hand = (FILE *)-1;
      } else {
         add_debug( "Got log %s ...\n", pl );
         oi( "Log file " );
         oi( pl );
         oi( MEOR );
      }
   }
}

void oi( PTSTR buf )
{
   if( g_do_logfile[0] ) {
      // add to a log file
      if( log_hand == 0 )
         open_log_file();
      if( log_hand && (log_hand != (FILE *)-1) ) {
         int len = (int)strlen(buf);
         int out = (int)fwrite(buf,1,len,log_hand);
         if (len != out) {
            fclose(log_hand);
            log_hand = (FILE *)-1;
         }
      }
   }
   if( VERB1 ) {
      printf("%s",buf);
   }
}

#define  MXIO  512
size_t prt( PTSTR ps )
{
   static TCHAR _s_prtbuf[MXIO+8];
   PTSTR  pb = _s_prtbuf;
   size_t len = strlen(ps);
   size_t   i, out, tout;
   int   c,d;
   d = 0;
   out = tout = 0;
   for( i = 0; i < len; i++ ) {
      c = ps[i];
      if( c == '\n' ) {
         if( d != '\r' ) {
            pb[out++] = '\r';
         }
      }
      pb[out++] = (TCHAR)c;
      d = c;
      if( out >= MXIO ) {
         pb[out] = 0;
         tout += out;
         oi(pb);
         out = 0;
      }
   }
   if(out) {
      pb[out] = 0;
      tout += out;
      oi(pb);
   }
   return tout;
}

int MCDECL sprtf( PTSTR ps, ... )
{
   static TCHAR _s_sprtfbuf[1024];
   PTSTR  pb = &_s_sprtfbuf[0];
   int   i;
   va_list arglist;
   va_start(arglist, ps);
   i = vsprintf( pb, ps, arglist );
   va_end(arglist);
   prt(pb);
   return i;
}

// eof - xprt.c
