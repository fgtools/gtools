// xutil.cxx

#include "xdelete.h"

static TCHAR _s_buf[1024];
static TCHAR _s_buf2[1024];
static TCHAR _s_buf3[1024];

PTSTR get_tmp_buf( void )
{
   return _s_buf;
}
PTSTR get_tmp_buf2( void )
{
   return _s_buf2;
}
PTSTR get_tmp_buf3( void )
{
   return _s_buf3;
}

/* ======================================================================
   nice_num = get nice number, with commas
   given a destination buffer,
   and a source buffer of ascii
   NO CHECK OF LENGTH DONE!!! assumed destination is large enough
   and assumed it is a NUMBER ONLY IN THE SOURCE
   ====================================================================== */
void nice_num( char * dst, char * src ) // get nice number, with commas
{
   size_t i;
   size_t len = strlen(src);
   size_t rem = len % 3;
   size_t cnt = 0;
   for( i = 0; i < len; i++ )
   {
      if( rem ) {
         *dst++ = src[i];
         rem--;
         if( ( rem == 0 ) && ( (i + 1) < len ) )
            *dst++ = ',';
      } else {
         *dst++ = src[i];
         cnt++;
         if( ( cnt == 3 ) && ( (i + 1) < len ) ) {
            *dst++ = ',';
            cnt = 0;
         }
      }
   }
   *dst = 0;
}

// test if string s is anywhere in str - case insensitive
int InStri( char * str, char * s )
{
   int bret = 0;
   int i = (int)strlen(str);
   int j = (int)strlen(s);
   if( j == i ) {
      if(stricmp(str,s) == 0 )
         bret = 1;
   } else if ( j < i ) {
      int k = toupper(*s);
      int x = i - j + 1;
      int m, o;
      char *a1, *a2;
      for( m = 0; m < x; m++ )
      {
          a1 = &str[m];
         if( toupper(str[m]) == k ) {
            // found first
            int n = 1;
            o = m;
            for( ; n < j; n++ ) {
               m++;
               a1 = &str[m];
               a2 = &s[n];
               if( toupper(str[m]) != toupper(s[n]) )
                  break;
            }
            if( n == j )
               return 1;
            m = o;  // restore pointer
         }
      }
   }
   return bret;
}



// eof - xutil.cxx
