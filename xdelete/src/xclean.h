// xclean.h - clean up header ...
#ifndef __xclean_h__
#define __xclean_h__

typedef struct tagCLEAN {
   TCHAR * clean_mask2;
   DWORD clean_counter;
}CLEAN, * PCLEAN;

// data
//extern CLEAN clean_masks[];
extern CLEAN clean_masks2[];  // FIX20080531 - added fnmatch (from grep)

// services
extern BOOL  IsInCleanList( PTSTR pf, PTSTR pdir, MyDirEnt *de );
extern BOOL xWildFileCompare( PTSTR f1, PTSTR f2 );
extern void Add_Clean_Exts( PTSTR pb );
// FIX20090305 - add more clean ouput, if VERB2
extern void show_clean_counts( void );

#endif // #ifndef __xclean_h__
// eof - xclean.h

