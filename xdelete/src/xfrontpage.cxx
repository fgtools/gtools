
#include "xdelete.h"

// my @excl_list = qw( _derived _private _vti_cnf _vti_pvt );
CLEAN clean_fp[] = {
   { "_derived", 0 },
   { "_private", 0 },
   { "_vti_cnf", 0 },
   { "_vti_pvt", 0 },
   { 0,          0 }

};

void show_frontpage_help( void )
{
   PCLEAN pc = &clean_fp[0];
   sprtf( "The -p [FrontPage] option will delete all the following folders ..."MEOR );
   while( pc->clean_mask2 )
   {
      sprtf( pc->clean_mask2 );
      sprtf( " " );
      pc++;
   }
   sprtf( " usually hidden!"MEOR );
}

int   is_frontpage_dir( char * pd )
{
   PCLEAN pc = &clean_fp[0];
   while( pc->clean_mask2 )
   {
      if( strcmpi( pd, pc->clean_mask2 ) == 0 ) {
         pc->clean_counter++;
         return 1;
      }
      pc++;
   }
   return 0;
}

void  process_fp_dir( char * pd, int level )
{
   MyDirEnt * de;
   MyDir * pdir = MyOpenDir( pd );
   char * nxt = (char *)malloc( MY_NAME_MAX );
   CHKMEM(nxt);
   if( VERB9 ) sprtf( "Processing folder [%s]"MEOR, pd);
   if( pdir ) {
      de = MyReadDir( pdir );
      while(de) {
         if( de->de_isdir ) {
            if( !( (strcmp(de->de_name,".")  == 0) ||
               (strcmp(de->de_name,"..") == 0) ) ) {
               strcpy( nxt, pd );
               strcat( nxt, "\\");
               strcat(nxt, de->de_name);
               if( is_frontpage_dir( de->de_name ) ) {
                  if( VERB9 ) sprtf( "Adding folder [%s]"MEOR, nxt);
                  add_2_list_if_new( &g_pINDIR, nxt, "INDIR" );
               }
               process_fp_dir( nxt, level + 1 );
            }
         }
         de = MyReadDir( pdir );
      }
      MyCloseDir( pdir );
   } else {
      if( VERB1 ) sprtf( "WARNING: Failed to open directory [%s]"MEOR, pd );
   }
   free(nxt);
}


void  add_frontpage_dirs( void )
{
   PMYLIST pn;
   PMYLIST ptmp = 0;
   int   max = get_list_count( g_pINDIR );
   int   i;

   pn = get_list_item( g_pINDIR, 0 );  // get FIRST folder input
   sprtf( "Found -p = DELETE FRONTPAGE DIRECTORIES in %s folder%s. (%d)"MEOR,
      pn->name,
      (max == 1 ? "" : "s"),
      max );
   for( i = 0; i < max; i++ ) {
      pn = get_list_item( g_pINDIR, i );
      add_2_list_if_new( &ptmp, pn->name, "TMPLIST" );
   }
   if(max && VERB5)
   {
      sprtf( "Removing previous %d IN-FOLDER list ..."MEOR, max );
      for( i = 0; i < max; i++ ) {
         pn = get_list_item( g_pINDIR, i );
         sprtf( "%s"MEOR, pn->name );
      }
   }
   kill_a_list( &g_pINDIR );   // remove this INPUT
   for( i = 0; i < max; i++ ) {
      pn = get_list_item( ptmp, i );
      process_fp_dir( pn->name, 0 );
   }
   kill_a_list( &ptmp );   // remove this INPUT

   // AFTER FIX UP = enumerate user given, generate list,
   // and DELETE the original user list.

   // THEN SHOW FINAL DELETION LIST
   // =============================
   max = get_list_count( g_pINDIR );
   pn = get_list_item( g_pINDIR, 0 );  // get FIRST folder input
   sprtf( "Found %d *** FRONTPAGE DIRECTORIES *** to delete ..."MEOR,
      max );
}

// eof - xfrontpage.c
