
// module: xhelp.c - command line handling

#include "xdelete.h"

// option variables
int   g_do_delete[2]    = { 0, 0 };
int   g_do_clean[2]    = { 0, 0 };
int   g_verbal[2]       = { 1, 1 };
int   g_tobin[2]        = { 0, 0 };
int   g_do_recursive[2] = { 0, 0 };
int   g_del_folders[2]  = { 0, 0 };
int   g_do_logfile[2]   = { 0, 0 };
int   g_do_modify[2]    = { 0, 0 }; // modify to READ/WRITE before delete
int   g_do_frontpage[2] = { 0, 0 }; // -p delete frontpage folders

// FIX20080906 added "-NO-confirmation-please"
BOOL please_NO_confirmation = FALSE;

// forward references
int give_help( TCHAR * * argv, int * pint );
int tog_opt( TCHAR * * argv, int * pint );
int set_iopt( TCHAR * * argv, int * pint );

#define MX_OPTS 11
// OPTS pgm_options[MX_OPTS];
OPTS pgm_options[MX_OPTS] = {
   { '?', Opt_Help, "this brief help.", give_help, 0, 0, 0 },
   { 'c', Opt_Bool, "Clean MSVC build files.", tog_opt, &g_do_clean[0], "msvc", "msvc" },
   { 'd', Opt_Bool, "Toggle do actual delete", tog_opt, &g_do_delete[0], "delete", "del" },
   { 'f', Opt_Bool, "Toggle folder delete.",   tog_opt, &g_del_folders[0], "folders", "fold" },
   { 'l', Opt_Bool, "Toggle log file, templog.txt.",   tog_opt, &g_do_logfile[0], "logfile", "log" },
   { 'm', Opt_Bool, "Toggle modify to READ/WRITE to allow delete.", tog_opt, &g_do_modify[0], "modify", "mod" },
   { 'p', Opt_Bool, "Toggle special FrontPage folder delete.", tog_opt, &g_do_frontpage[0], "frontpage", "fp" },
   { 'r', Opt_Bool, "Toggle recursive into folders.", tog_opt, &g_do_recursive[0], "recursive", "recur" },
   { 's', Opt_Bool, "Toggle save to recyle bin, if possible.", tog_opt, &g_tobin[0], "recycle", "recyc" },
   { 'v', Opt_Int,  "Set verbosity level - range 0 - 9.", set_iopt, &g_verbal[0], "verbal", "verb" },
   { 0,   Opt_None,  0,                         0,       0, 0, 0  },
};

int	GetInputFile( PTSTR lpf );

void show_clean_help( void )
{
   PCLEAN pc = &clean_masks2[0];
   sprtf( "The -c [CLEAN] option will delete all files matching the following :-"MEOR );
   while( pc->clean_mask2 )
   {
      sprtf( pc->clean_mask2 );
      sprtf( " " );
      pc++;
   }
   sprtf( MEOR );
}

void show_help( TCHAR * prog )
{
   static TCHAR _s_obuf[264];
   TCHAR * ps = _s_obuf;
   POPTS popts = &pgm_options[0];
   int   i, j, k;
   int * pi;
   k = g_verbal[0];
   g_verbal[0] = 1;
   sprtf( "%s - version %s, of %s, %s\n", prog, VERS, __DATE__ , __TIME__ );
   sprtf( "Usage: [switches] <directory> (or @input_file)\n" );
   sprtf( "Switches:\n");
   while( popts->opt_hand ) {
      pi = popts->opt_var;
      sprintf(ps, "   -%c", popts->the_option );
      if( popts->opt_type == Opt_Int )
         strcat(ps, "[n] " );
      else
         strcat(ps, "    " );
      strcat(ps, popts->opt_help );
      if( popts->opt_type == Opt_Int ) {
         i = *pi;
         pi++;
         j = *pi;
         if( i == j )
            sprintf( EndBuf(ps), " (Default=%d)", i );
         else
            sprintf( EndBuf(ps), " (Default=%d, Now=%d)", j, i );
      } else if( popts->opt_type == Opt_Bool ) {
         i = *pi;
         pi++;
         j = *pi;
         if( i == j )
            sprintf( EndBuf(ps), " (Default=%s)", (j ? "On" : "Off") );
         else
            sprintf( EndBuf(ps), " (Default=%s, Now=%s)", (j ? "On" : "Off"), (i ? "On" : "Off"));
      }
      sprtf("%s"MEOR, ps);
      popts++;
   }

   // FIX20090411 -dir=<directory> - alternate for enterning <directory>
   sprtf( "   -dir=<dir> Alternate entry of directory name."MEOR );

   show_clean_help();
   show_frontpage_help();
   // 20080906 - add -please-NO-prompt, to avoid the confirmation
   sprtf( "Special NOT RECOMMENDED switch, which is self descriptive!"MEOR );
   sprtf( "   -NO-confirmation-please[=nn] : It is long and case sensitive, to remind of the DANGER!"MEOR);
   sprtf( "But even with this switch, the program watches the keyboard for %d seconds, before proceeding."MEOR,
      ( 2 * ( default_wait / 1000 ) ));
   // FIX20090112 - add nn=secs to HELP output
   sprtf( "Unless the timeout(x2) is over-ridden with a number of seconds value nn!"MEOR );

   g_verbal[0] = k;
}

int give_help( TCHAR * * argv, int * pint )
{
   show_help( g_pProg );
   pgm_exit(PEXIT_SUCCESS);
   return 0;
}

int tog_opt( TCHAR * * argv, int * pint )
{
   int i = *pint;
   if(i)
      i = 0;
   else
      i = 1;
   *pint = i;
   return 0;
}

int set_iopt( TCHAR * * argv, int * pint )
{
   TCHAR * arg = *argv;
   int   i = *pint;
   if( ISNUM(*arg) ) {
      i = atoi(arg);
      while( ISNUM(*arg) )
         arg++;
      *argv = arg;
   }
   *pint = i;
   return 0;
}

int ends_with_space( TCHAR * pb )
{
   size_t len = strlen(pb);
   if(len) {
      if( pb[len-1] <= ' ' ) {
         return 1;
      }
      return 0;
   }
   return 1; // no length is LIKE a space
}

size_t get_option_list( TCHAR * pb )
{
   size_t size = strlen(pb);
   POPTS pops = &pgm_options[0];
   TCHAR * pch;
   PINT pint;
   // process ONLY the "ON" options first
   while( pops->the_option ) {
      pch = pops->opt_name;
      pint = pops->opt_var;
      if( pch && *pch && pint ) {
         if( ( pops->opt_type == Opt_Bool ) && *pint ) {
            if( !ends_with_space(pb) )
               strcat(pb," ");
            strcat(pb,pch);
            strcat(pb,"=");
            strcat(pb,"ON");
         }
      }
      pops++;
   }
   pops = &pgm_options[0]; // reset to START
   while( pops->the_option ) {
      pch = pops->opt_name;
      pint = pops->opt_var;
      if( pch && *pch && pint ) {
         switch(pops->opt_type)
         {
         case Opt_Bool:
            if(*pint) {
               // HAS BEEN DONE strcat(pb,"ON");
            } else {
               if( !ends_with_space(pb) )
                  strcat(pb," ");
               strcat(pb,pch);
               strcat(pb,"=");
               strcat(pb,"OFF");
            }
            break;
         case Opt_Int:
            if( !ends_with_space(pb) )
               strcat(pb," ");
            strcat(pb,pch);
            strcat(pb,"=");
            sprintf(EndBuf(pb), "%d", *pint );
            break;
         default:
            strcat(pb,"Uncased!"); // never happen!
            break;
         }
      }
      pops++;
   }
   return (strlen(pb) - size);
}

// FIX20070815 - add this SHORT option LIST
size_t get_short_option_list( TCHAR * pb )
{
   size_t size = strlen(pb);
   POPTS pops = &pgm_options[0];
   TCHAR * pch;
   PINT pint;
   // process ONLY the "ON" options first
   while( pops->the_option ) {
      pch = pops->opt_sn;
      pint = pops->opt_var;
      if( pch && *pch && pint ) {
         if( ( pops->opt_type == Opt_Bool ) && *pint ) {
            if( !ends_with_space(pb) )
               strcat(pb," ");
            strcat(pb,pch);
            strcat(pb,"=");
            strcat(pb,"on");
         }
      }
      pops++;
   }
   pops = &pgm_options[0]; // reset to START
   while( pops->the_option ) {
      pch = pops->opt_sn;
      pint = pops->opt_var;
      if( pch && *pch && pint ) {
         switch(pops->opt_type)
         {
         case Opt_Bool:
            if(*pint) {
               // HAS BEEN DONE strcat(pb,"ON");
            } else {
               if( !ends_with_space(pb) )
                  strcat(pb," ");
               strcat(pb,pch);
               strcat(pb,"=");
               strcat(pb,"off");
            }
            break;
         case Opt_Int:
            if( !ends_with_space(pb) )
               strcat(pb," ");
            strcat(pb,pch);
            strcat(pb,"=");
            sprintf(EndBuf(pb), "%d", *pint );
            break;
         default:
            strcat(pb,"Uncased!"); // never happen!
            break;
         }
      }
      pops++;
   }
   return (strlen(pb) - size);
}

BOOL IsAllNums( char * arg )
{
   size_t len = strlen(arg);
   size_t i;

   if( len == 0 )
      return FALSE;  // well, there are NO number, so cna not be ALL numbers

   for( i = 0; i < len; i++ )
   {
      if( !ISNUM(arg[i]) )
         return FALSE;
   }
   return TRUE;
}

int process_args( int argc, TCHAR * * argv )
{
   int   i, c;
   POPTS popts;
   for( i = 1; i < argc; i++ ) {
      TCHAR * arg = argv[i];
      if ( strncmp(arg,"-NO-confirmation-please", 23) == 0)
      {
         arg += 23;
         please_NO_confirmation = TRUE;
         if( *arg )
         {
            if( *arg == '=' )
            {
               arg++;
               if( IsAllNums(arg) )
               {
                  default_wait = (atoi(arg) * 1000);
               }
               else
                  goto Bad_Arg1;
            }
            else
            {
Bad_Arg1:
               arg = argv[i];
               sprtf("ERROR: Bad Argument! [%s]. Aborting ..."MEOR, arg );
               sprtf("Can ONLY be -NO-confirmation-please[=<nums>]!"MEOR);
               pgm_exit(PEXIT_ERROR);
            }
         }

         sprtf( "NOTE: NO-confirmation-please found, with timeout %d seconds."MEOR,
            default_wait / 1000 );
         continue;
      }
      // FIX20090411 -dir=<directory> - alternate for enterning <directory>
      if (( strncmp(arg,"-dir=", 5) == 0) && arg[5] ) {
         add_2_list( &g_pINDIR, &arg[5], "INDIR" );
         continue;
      }
      if( *arg == '-' ) {
         arg++;
         while( *arg ) {
            popts = &pgm_options[0];
            c = tolower(*arg);
            while( popts->opt_hand ) {
               if( c == popts->the_option ) {
                  arg++;
                  popts->opt_hand( &arg, popts->opt_var );
                  break;
               }
               popts++; // move to NEXT ...
            }
            if( !popts->opt_hand ) { // ran out of otpions
               // check for -h, or even --help ...
               if( ( c == 'h' ) ||
                   ( ( c == '-' ) && ( tolower( arg[1] ) == 'h' ) ) ) {
                  give_help( 0, 0 );
               } else {
                  // out of OPTIONS
                  // show_help( g_pProg );
                  sprtf( "ERROR: Invalid argument [%c] in [%s] ... use -? for HELP ... exiting ...\n",
                     c, argv[i] );
                  pgm_exit(PEXIT_ERROR);
               }
            }
         }
      } else if( *arg == '@' ) {
         arg++;
         if( GetInputFile( arg ) ) {
            sprtf( "ERROR: Invalid input file [%s] ... exiting ...\n",
               argv[i] );
            pgm_exit(PEXIT_ERROR);
         }
      } else {
#ifdef  MULTI_DIRECTORIES
// extern PMYLIST  g_pINDIR;
// extern TCHAR * g_pCurDir;
         add_2_list( &g_pINDIR, arg, "INDIR" );
#else // !#ifdef  MULTI_DIRECTORIES
// extern TCHAR * g_dir = 0;
         if( g_dir ) {
            sprtf( "ERROR: Already have directory [%s]!\n"
               "This [%s] appears to be a second?\n"
               "Only ONE(1) presently allowed.\n", g_dir, arg );
            pgm_exit(PEXIT_ERROR);
         }
         g_dir = strdup( arg );
#endif   // #ifdef  MULTI_DIRECTORIES y/n
      }
   }

   return 0;
}

// allow an input file
#define  VH(a) ( a && (a != INVALID_HANDLE_VALUE) )
#define  MXARGS   32
#define  prt   sprtf
#define  MCRLF    MEOR
// #define  Exit_Error     -1

HANDLE	OpenForRead( PTSTR lpf )
{
	HANDLE	hFile;
#if (defined(_MSC_VER) && !defined(USE_PORTABLE_FUNCS))
	hFile = CreateFile( lpf,	// pointer to name of the file
		GENERIC_READ,			// access (read-write) mode
		0,						// share mode
		NULL,					// pointer to security attributes
		OPEN_EXISTING,			// how to create
		FILE_ATTRIBUTE_NORMAL,	// file attributes
		NULL );					// handle to file with attributes to 
                               // copy
#else // !_MSC_VER
    printf("WARNING: OpenForRead() needs to be ported!\n");
    hFile = (HANDLE)fopen( lpf, "rb" );
#endif // _MSC_VER y/n
	return hFile;
}

DWORD	RetFileSize( HANDLE hFile )
{
	DWORD	dwRet = 0;
	if( VH( hFile ) )
	{
#if (defined(_MSC_VER) && !defined(USE_PORTABLE_FUNCS))
    	DWORD	dwSize1, dwSize2;
		dwSize1 = GetFileSize( hFile, &dwSize2 );
		if( ( dwSize2 == 0 ) &&
			( dwSize1 ) &&
			( dwSize1 != (DWORD)-1 ) )
		{
			dwRet = dwSize1;
		}
#else
        printf("WARNING: port pf RetFileSize() needs to be checked!\n");
        FILE *fp = (FILE *)hFile;
        fseek(fp, 0L, SEEK_END);
        dwRet = (DWORD)ftell(fp);
        fseek(fp, 0L, SEEK_SET);
#endif		
	}
	return dwRet;
}

#if (defined(_MSC_VER) && !defined(USE_PORTABLE_FUNCS))
#define MFREE(a) LocalFree(a)
#define MALLOC(a,b) LocalAlloc(a,b)
#else
#define MFREE(a) free(a)
#define MALLOC(a,b) malloc(b)
#endif

BOOL XReadFile( HANDLE hFile, LPSTR buf, DWORD size1, DWORD *psize2, void *vp )
{
#if (defined(_MSC_VER) && !defined(USE_PORTABLE_FUNCS))
    if ( ReadFile( hFile, buf, size1, psize2, (LPOVERLAPPED)vp )) {
        return TRUE;
    }
#else
    //printf("WARNING: port of ReadFile needs to be checked!\n");
    FILE *fp = (FILE *)hFile;
    DWORD ret = (DWORD)fread( buf, 1, size1, fp );
    if (ret == size1)
        return TRUE;
#endif
    return FALSE;
}

void XCLoseFile( HANDLE hFile )
{
#if (defined(_MSC_VER) && !defined(USE_PORTABLE_FUNCS))
    CloseHandle( hFile );
#else
    fclose((FILE *)hFile);
#endif
}

int	GetInputFile( PTSTR lpf )
{
	int	bRet = TRUE;	// begin with error
	HANDLE	hFile;
	DWORD	dwSize1, dwSize2 = 0;
	DWORD	dwAlloc;
	LPSTR	lpb;

	hFile = OpenForRead( lpf );
	if( VH( hFile ) )
	{
		dwSize1 = RetFileSize( hFile );
		if( ( dwSize2 == 0 ) &&
			( dwSize1 ) &&
			( dwSize1 != (DWORD)-1 ) )
		{
			dwAlloc = ( ( dwSize1 + 1 ) + ( MXARGS * sizeof(LPVOID) ) );
			lpb = (LPSTR)MALLOC( LPTR, dwAlloc );
         CHKMEM(lpb);
			if( lpb )
			{
				if( ( XReadFile( hFile,	// handle of file to read
					lpb,	// pointer to buffer that receives data
					dwSize1,	// number of bytes to read
					&dwSize2,	// pointer to number of bytes read
					NULL ) ) &&
					( dwSize1 == dwSize2 ) )
				{

					LPSTR *	lpargv;
					LPSTR	lpcmd;
					int		argc, k, j, l;
					char	c;

					lpargv = (LPSTR *) (LPSTR)(lpb + dwSize1 + 1);
					argc = 0;
					lpargv[argc++] = g_pProg; // &szModule[0];
					k = 0;
					for( dwSize2 = 0; dwSize2 < dwSize1; dwSize2++ )
					{
						c = lpb[dwSize2];
						if( c == ';' )
						{
							// Skip comments in file
Skip:
							dwSize2++;
							for( ; dwSize2 < dwSize1; dwSize2++ )
							{
								c = lpb[dwSize2];
								if( c == '\n' )
								{
									//dwSize2++;
									break;
								}
							}
						}
						else if( c > ' ' )
						{
							j = k;
							lpcmd = &lpb[k];
							lpb[k++] = c;
							dwSize2++;
							for( ; dwSize2 < dwSize1; dwSize2++ )
							{
								c = lpb[dwSize2];
								if( ( c == ';' ) ||
									( c < ' ' ) )
								{
									break;
								}
								else
								{
									lpb[k++] = c;
								}
							}
							l = k - 1;
							while( ( l > j ) &&
								( lpb[l] <= ' ' ) )
							{
								l--;
							}
							l++;
							lpb[l] = 0;
							k = l + 1;
							//lpargv[argc++] = &lpb[j];
							lpargv[argc++] = lpcmd;
							if( argc >= MXARGS )
							{
								prt( MCRLF"ERROR: Input file contains too many arguments!" );
								//gIRet = Exit_Error;
								//PgmExit(TRUE);
                        pgm_exit(PEXIT_ERROR); // Exit_Error);
							}
							if( c == ';' )
								goto Skip;
						}
					}
					// processed all the file
					if( argc > 1 )
					{
                  bRet = process_args( argc, lpargv );
						//ProcessCommand( pWs, argc, lpargv );
						//bRet = 0;
					}
					else
					{
                  // FIX20060721 - quietly IGNORE no arguments found in file!
                  bRet = 0;   // thus set NO ERROR
                  sprtf("WARNING: Input file [%s] contains no arguments ..."MEOR,
                     lpf );
						//LocalFree(lpb);
						//CloseHandle(hFile);
						//lpb = 0;
						//hFile = 0;
						//prt( "ERROR: Input file error!"MCRLF );
						////gIRet = Exit_Error;
						////PgmExit(TRUE);
      //            pgm_exit(PEXIT_ERROR); // Exit_Error
					}
				}
				if( lpb ) {
					MFREE(lpb);
				}
         } 
		}

		if( VH(hFile) ) {
            XCLoseFile( hFile );
	    }
	}

	return bRet;

}

// eof - xhelp.c
