// xprt.h - output handling ...
#ifndef __xprt_h__
#define __xprt_h__
#ifdef _MSC_VER
#define MCDECL _cdecl
#define  MEOR  "\r\n"
#else
#define MCDECL
#define  MEOR  "\n"
#endif

extern int MCDECL sprtf( PTSTR ps, ... );

#endif // #ifndef __xprt_h__
// eof - xprt.h

