// module: exdelete.cxx

#include <iostream>
#include <tchar.h>

//#include "xdelete.h"
//#include <stdio.h>
//#include <stdlib.h>
//#include <stdarg.h>  /* may need <varargs.h> for Unix V */
//#include <direct.h>
//#include <sys/types.h>
//#include <sys/stat.h>
//#include <windows.h>
#include <conio.h>

#using <mscorelib.dll>

// This example demonstrates the Console.KeyAvailable property.
using namespace System;
using namespace System::Threading;
int test_main()
{
   ConsoleKeyInfo cki;
   do
   {
      Console::WriteLine( "\nPress a key to display; press the 'x' key to quit." );
      
      // Your code could perform some useful task in the following loop. However, 
      // for the sake of this example we'll merely pause for a quarter second.
      while ( Console::KeyAvailable == false )
            Thread::Sleep( 250 );
      cki = Console::ReadKey( true );
      Console::WriteLine( "You pressed the '{0}' key.", cki.Key );
   }
   while ( cki.Key != ConsoleKey::X );
}

int get_keyboard_character( void )
{
   int chr;

   chr = _getch();

   return chr;
}



// eof - exdelete.cxx

