// xhelp.h - command line handling ...
#ifndef __xhelp_h__
#define __xhelp_h__

// option variables
extern int  g_do_delete[];
extern int  g_verbal[];
extern int  g_tobin[];
extern int  g_do_recursive[];
extern int  g_del_folders[];
extern int  g_do_logfile[];
extern int  g_do_clean[];
extern int  g_do_modify[]; // = { 0, 0 }; // modify to READ/WRITE before delete
extern int  g_do_frontpage[]; // = { 0, 0 }; // -p delete frontpage folders

typedef enum {
   Opt_None,
   Opt_Help,
   Opt_Bool,
   Opt_Int
}OptType;

typedef int (*OPTHAND) ( TCHAR * * argv, int * pint );

typedef struct tagOPTS {
   int the_option;
   OptType  opt_type;   // type of option
   TCHAR * opt_help;    // description
   OPTHAND  opt_hand;
   int * opt_var;
   TCHAR * opt_name; // FULL name for option
   TCHAR * opt_sn;   // FIX20070815 - add SHORT name for option
}OPTS, * POPTS;

extern int process_args( int argc, TCHAR * * argv );
extern void show_help( TCHAR * prog );
extern size_t get_option_list( TCHAR * pb );
extern size_t get_short_option_list( TCHAR * pb );

#endif // #ifndef __xhelp_h__
// eof - xhelp.h
