// xlist.h - command line handling ...
#ifndef __xlist_h__
#define __xlist_h__

// manifest constants
#define  MX_FILE_NM     42
#define  MX_FILE_SZ     52
#define  ASSUME_FOLDER_SIZE   256   // this varies, depending on file system used

struct _MYLIST;
typedef struct _MYLIST MYLIST;
typedef MYLIST * PMYLIST;
struct _MYLIST {
   PMYLIST pnext; // pointer to NEXT, if any
   int     done;  // done this folder
   int     isdir; // is a directory
#ifdef USE_64BIT_COUNTERS
   ULARGE_INTEGER ul_size64;
#else
   unsigned long ul_size;  // size
#endif
   int     isclean;  // added because of -c CLEAN
   time_t  time;  // time
#ifdef   _MSC_VER
   DWORD    attrib;
   DWORD bSectorsPerCluster, bBytesPerSector, bNumberOfFreeClusters, bTotalNumberOfClusters;
   ULARGE_INTEGER bFreeBytesAvailable, bTotalNumberOfBytes, bTotalNumberOfFreeBytes;
   int      bshwn;
#endif // _MSC_VER
   char    name[1]; // buffer for full item name
};

#define  traverse_my_list(ph, pn) for( pn = ph; pn != 0; pn = pn->pnext )

// FIX20070815 - check if in LIST
extern PMYLIST  is_in_list( PMYLIST * pList, char * path, char * lname );
extern PMYLIST  add_2_list_if_new( PMYLIST * pList, char * path, char * lname );
extern PMYLIST  g_pPATHS, g_pFILES;
extern PMYLIST g_TEMP_Skipped, g_TEMP_Deleted;

extern void kill_lists( void );
extern void show_lists( void );
extern void get_file_stg( PTSTR ps, PMYLIST pn );
extern PMYLIST get_list_last( PMYLIST head );
extern int get_list_count( PMYLIST head );
extern PMYLIST get_list_before( PMYLIST head, PMYLIST nxt );
extern void  add_2_end( PMYLIST head, PMYLIST nxt, char * lname );
extern PMYLIST  add_2_list( PMYLIST * pList, char * path, char * lname );
extern PMYLIST  add_2_paths( char * path );
extern PMYLIST  add_2_files( char * path );
extern TCHAR * get_k_num( unsigned long byts );
extern PMYLIST  get_list_item( PMYLIST head, int item );
extern TCHAR * get_k_num64( ULARGE_INTEGER uli );
extern void kill_a_list( PMYLIST * phead );

extern PMYLIST  add_2_TEMP_Skipped( char * path );
extern PMYLIST  add_2_TEMP_Deleted( char * path );

#endif // #ifndef __xlist_h__
// eof - xlist.h
