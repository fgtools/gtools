/*\
 * xkey.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/
#include "xdelete.h"
#ifdef _MSC_VER
#include <conio.h>
#else // !_MSC_VER
#include <unistd.h>  // for STDIN_FILENO
#include <termio.h> 
#include <fcntl.h>  // for F_GETFL
#endif // _MSC_VER

extern BOOL please_NO_confirmation; // FIX20080906 added "-NO-confirmation-please"

#define  VFH(a)   ( a && ( a != INVALID_HANDLE_VALUE ) )

int default_wait = 5000;  // ms to to wait
HANDLE ghStdOut = NULL;
HANDLE ghErrOut = NULL;
BOOL  gbRedON = FALSE;  // gotred - got redirection of stdout
BOOL  gbErrON = FALSE;  // got redirection of stderr

void pgm_init( void )
{
#ifdef _MSC_VER
   DWORD gdwMode;
   ghStdOut = GetStdHandle( STD_OUTPUT_HANDLE );   // Standard out
   ghErrOut = GetStdHandle( STD_ERROR_HANDLE  );   // error out
   if( VFH(ghStdOut) )
   {
      if( !GetConsoleMode( ghStdOut, &gdwMode ) )
         gbRedON = TRUE;
   }
   if( VFH(ghErrOut) )
   {
      if( !GetConsoleMode( ghErrOut, &gdwMode ) )
         gbErrON = TRUE;
   }
#else
   printf("PORT:WARNING: Checking for redirection needs to be ported!\n");
#endif
}

#ifdef _MSC_VER
#define SLEEP _sleep
#define GETCHAR _getch // returns immediately which is preferred, but NO stdin redirection!
// try #define GETCHAR getchar // equavalent to getc(...) using stdin - so redirection should work...
// UGH: Yes it does, but now requires an <enter> key to get the char ;=(((((((((((
#else
#define NANO_SECOND_MULTIPLIER  1000000  // 1 millisecond = 1,000,000 Nanoseconds
// #define SLEEP_INTERVAL  (55 * NANO_SECOND_MULTIPLIER)
#define SLEEP(a) {                                           \
    struct timespec _ts = {0, (a * NANO_SECOND_MULTIPLIER)}; \
    nanosleep(&_ts,NULL);                                    \
    }
#define GETCHAR getchar
#endif


#ifndef _MSC_VER
// a unix port of int _kbhit()
int _kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if(ch != EOF) {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}

#endif // !_MSC_VER

int check_key_available( void )
{
   int chr = _kbhit();
   return chr;
}

int get_keyboard_character( void )
{
   int chr, i;
   int   wait = default_wait;

   chr = 'N';

   if ( please_NO_confirmation && ( wait == 0 ) )
      chr = 'Y';

   while( wait )
   {
      i = check_key_available(); // check for key
      if( i )
      {
         // a keyboard character is waiting - get it
         chr = GETCHAR();
         return chr;
      }

      SLEEP(55);

      if( please_NO_confirmation )
      {
         if( wait > 55 )
         {
            wait -= 55;
         }
         else
         {
            chr = 'Y';  // simulate a 'Y' keyboard
            wait = 0;   // and the WAIT IS OVER
         }
      }
   }

   return chr;
}

#if 0 // 0000000000000000000000000000000000000000000000000000000

// this WAITS
int check_stdin_available( void )
{
   int iret = 0;
#ifdef _MSC_VER
   char chBuffer[1];
   DWORD cRead;
   OVERLAPPED ol;
   HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE);

   ZeroMemory( &ol, sizeof(OVERLAPPED));
   if (! ReadFile( 
            hStdin,    // input handle 
            (LPVOID)chBuffer,  // buffer to read into 
            sizeof(chBuffer), // size of buffer 
            &cRead,    // actual bytes read 
            &ol) )    // not overlapped 
   {
      DWORD err = GetLastError();
      if( err == ERROR_IO_PENDING )
         iret = 1;
      return 0;
   }
   iret = chBuffer[0];
#endif  
   return iret;
}

// this does NOT work!!!
int check_CONIN_available( void )
{
#ifdef _MSC_VER
   FILE * hStdin = fopen( "CONIN$", "r" );
   if ( !hStdin )
      return 0;
   if ( feof(hStdin) )
      return 0;
   return 1;
#else
    return 0;
#endif   
}

#endif // 0000000000000000000000000000000000000000000000000

// eof - xkey.cxx
