# xdelete - 20140823 - 20140218
####################################################################################
### SEE SPECIFIC INSTALL LOCATION BELOW - ADJUST TO SUIT YOUR ENVIRONMNET/DESIRE ###
####################################################################################

cmake_minimum_required (VERSION 2.8)

project (xdelete)

option( USE_PORTABLE_FUNCS "Set ON to use portable functions replacing WIN32 API"  ON )

if (CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(IS_64BIT_BUILD 1)
endif ()


# to distinguish between debug and release libraries
if (MSVC)
    set( CMAKE_DEBUG_POSTFIX "d" )
endif ()

if(CMAKE_COMPILER_IS_GNUCXX)
    set( WARNING_FLAGS -Wall )
endif(CMAKE_COMPILER_IS_GNUCXX)

if (CMAKE_CXX_COMPILER_ID STREQUAL "Clang") 
   set( WARNING_FLAGS "-Wall -Wno-overloaded-virtual" )
endif() 

if(WIN32 AND MSVC)
    # turn off various warnings
    set(WARNING_FLAGS "${WARNING_FLAGS} /wd4996")
    # foreach(warning 4244 4251 4267 4275 4290 4786 4305)
    #     set(WARNING_FLAGS "${WARNING_FLAGS} /wd${warning}")
    # endforeach(warning)

    set( MSVC_FLAGS "-DNOMINMAX -D_USE_MATH_DEFINES -D_CRT_SECURE_NO_WARNINGS -D_SCL_SECURE_NO_WARNINGS -D__CRT_NONSTDC_NO_WARNINGS" )
    # if (${MSVC_VERSION} EQUAL 1600)
    #    set( MSVC_LD_FLAGS "/FORCE:MULTIPLE" )
    # endif (${MSVC_VERSION} EQUAL 1600)
    # set( NOMINMAX 1 )
else()
    set( WARNING_FLAGS "${WARNING_FLAGS} -Wno-write-strings -Wno-trigraphs -Wno-unused-variable -Wno-unused-value" )
endif()

set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${WARNING_FLAGS} ${MSVC_FLAGS} -D_REENTRANT" )
set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${MSVC_LD_FLAGS}" )

if (USE_PORTABLE_FUNCS)
    add_definitions( -DUSE_PORTABLE_FUNCS=1 )
endif ()

if (IS_64BIT_BUILD OR NOT MSVC)
    set(name xdelete)
else ()
    set(name xdelete32)
endif ()
set(dir src)
set(${name}_SRCS
    ${dir}/xclean.cxx
    ${dir}/xdelete.cxx
    ${dir}/xdir.cxx
    ${dir}/xfnmatch.cxx
    ${dir}/xfrontpage.cxx
    ${dir}/xhelp.cxx
    ${dir}/xkey.cxx
    ${dir}/xlist.cxx
    ${dir}/xprt.cxx
    ${dir}/xutil.cxx
    )
set(${name}_HDRS
    ${dir}/xclean.h
    ${dir}/xdelete.h
    ${dir}/xdir.h
    ${dir}/xfnmatch.h
    ${dir}/xfrontpage.h
    ${dir}/xhelp.h
    ${dir}/xkey.h
    ${dir}/xlist.h
    ${dir}/xprt.h
    ${dir}/xutil.h
    ${dir}/xvers.h
    )
add_executable(${name} ${${name}_SRCS} ${${name}_HDRS})
if(add_LIBS)
    target_link_libraries( ${name} ${add_LIBS} )
endif ()
if (MSVC)
    set_target_properties(${name} PROPERTIES DEBUG_POSTFIX d)
    ###############################################################################
    ### NOTE SPECIFIC INSTALL LOCATION - ADJUST TO SUIT YOUR ENVIRONMNET/DESIRE ###
    ###############################################################################
    install (TARGETS ${name} DESTINATION C:/MDOS)
else ()
    install (TARGETS ${name} DESTINATION bin)
endif ()

# eof

