
README.txt - xdelete - 20140219 - 20060324


Overview:
========

           *** TAKE CARE ***

This little app IS VERY DANGEROUS. *** TAKE DUE CARE IN USE ***

Given a directory, and some option flags, it will proceed to DELETE 
ALL the files found in that directory, and in sub-directories, then 
proceeed to delete the directories themselves.

If you accidently point it to system essential folders, then your 
system may not run, after, and there is no way to recover without a 
full reinstall.

           *** TAKE CARE ***


Build:
=====

This app uses a cmake build system. If this is installed, and say MSVC 10 upwards,
then it should be built just by entering the 'build' directory and typing 'build-me'.

*********************************************
*** TAKE CARE WITH THE FINAL INSTALL STEP ***
*********************************************

In my system I place all such utility apps in a directory C:\MDOS, *AND* this 
directory has been added to my path.

YOU NEED TO ADJUST THE build-me.bat IF YOU WANT TO INSTALL IT ELSEWHERE!!!
==========================================================================

The build-me.bat STOPS before the install step, and can be aborted by 
Ctrl+C - all other keys and it will continue.

Care if you decide to install it in one of the Windows system paths. In 
later windows system this would need to be done with administrator priviledges.

Also be aware the cmake default windows install is into like -
C:\Program Files (x86)\xdelete

At present in Windows it compiles as a WIN32 app, but if the version of MSVC you 
have installed supports building WIN64, then adding the OPTION -
 -G "Visual Studio 10 Win64"
adjusted for the version of MSVC used should compile a WIN64 version.

 
Scope:
=====

This app ws built only in windows, and no attempt has been made to compile it in 
other OSES, but that should not be too difficult.

The code compiles as C++, but it is essentially just C.


Licence:
=======

GNU GPL Version 2

Copyright (C) Geoff R. McLane - 2006 - 2014

I would be happy to hear from you - email address given below - if you find it useful,
and especially if you find and fix any 'bugs'.


Help:
====
This is the -? help given by the app -

xdelete - version 1.0.11, of Feb 18 2014, 13:54:06
Usage: [switches] <directory> (or @input_file)
Switches:
   -?    this brief help.
   -c    Clean MSVC build files. (Default=Off)
   -d    Toggle do actual delete (Default=Off)
   -f    Toggle folder delete. (Default=Off)
   -l    Toggle log file, templog.txt. (Default=Off)
   -m    Toggle modify to READ/WRITE to allow delete. (Default=Off)
   -p    Toggle special FrontPage folder delete. (Default=Off)
   -r    Toggle recursive into folders. (Default=Off)
   -s    Toggle save to recyle bin, if possible. (Default=Off)
   -v[n] Set verbosity level - range 0 - 9. (Default=1)
   -dir=<dir> Alternate entry of directory name.
The -c [CLEAN] option will delete all files matching the following :-
*.old *.bak *.obj *.err *.pdb *.lst *.pch *.ilk *.ncb *.plg *.opt *.idb *.aps *.sbr *.suo *.manifest *.res *.user *.bsc
mt.dep BuildLog.htm *.sdf *.ipch
The -p [FrontPage] option will delete all the following folders ...
_derived _private _vti_cnf _vti_pvt  usually hidden!
Special NOT RECOMMENDED switch, which is self descriptive!
   -NO-confirmation-please[=nn] : It is long and case sensitive, to remind of the DANGER!
But even with this switch, the program watches the keyboard for 10 seconds, before proceeding.
Unless the timeout(x2) is over-ridden with a number of seconds value nn!

Enjoy ;=))


Geoff R. McLane
reports _at_ geoffair _dot_ info

# eof
