
README.txt - showlnk - 20140129


Overview:
========

This little app read, and dumps a Windows Desktop link (*.lnk) file.

I had tried to do this quite a long time back, but was not too successful
in 'guessing' the structures it contained.

This time a web search revealed lots MORE information, including an 
'official' Microsoft site. See -
 http://msdn.microsoft.com/en-us/library/dd891253.aspx
and perhaps others...

An alternate 'unofficial' source of information could be found here -
 http://fileanalysis.net/lnk/

And there is gives another link to an 'official' documentation site -
 http://msdn.microsoft.com/en-us/library/dd871305%28v=prot.20%29.aspx

It took some effort to put all this information into structures, get that right,
and then be able to 'walk' the structures in the file...

I may have got that right this time ;=))


Build:
=====

This app uses a cmake build system. If this is installed, and say MSVC 10 upwards,
then it should be built just by entering the 'build' directory and typing 'build-me'.

*********************************************
*** TAKE CARE WITH THE FINAL INSTALL STEP ***
*********************************************

In my system I place all such utility apps in a directory C:\MDOS, *AND* this 
directory has been added to my path.

YOU NEED TO ADJUST THE build-me.bat IF YOU WANT TO INSTALL IT ELSEWHERE!!!
==========================================================================

The build-me.bat STOPS before the install step, and can be aborted by 
Ctrl+C - all other keys and it will continue.

Care if you decide to install it in one of the Windows system paths. In 
later windows system this would need to be done with administrator priviledges.

Also be aware the cmake default windows install is into like -
C:\Program Files (x86)\showlnk

At present in Windows it compiles as a WIN32 app, but if the version of MSVC you 
have installed supports building WIN64, then adding the OPTION -
 -G "Visual Studio 10 Win64"
adjusted for the version of MSVC used should compile a WIN64 version.

 
Scope:
=====

Since this is a MS *.lnk file, this app is intended mainly for windows, so no 
attempt has been made to compile it in other OSES, but that should not be too difficult.

The code compiles as C++, but it is essentially just C.

It is quite 'messy' since it was developed by trial and error. And when I got 
it working the way I wanted it, no effort was made to tidy up the code, so there 
are quite a number of 'scraps' not used.


Licence:
=======

GNU GPL Version 2

Copyright (C) Geoff R. McLane - 2014

I would be happy to hear from you - email address given below - if you find it useful,
and especially if you find and fix any 'bugs'.


Help:
====
This is the -? help given by the app -

showlnk [Options] lnk_file [or directory] [or wild card] [more files/directories...]
Options
 --help (or -h or -?) = This help and exit.
 --recursive     (-r) = Process subdirectories, if a directory or wild card given.
 --link          (-l) = Dump link info section, if present.
 --verb[nn]      (-v) = Bump. or set verbosity to nn value.

 Basically just an exercise is 'decoding' a Microsoft LINK (shortcut) file. (*.lnk)
 and if verbosity set, then display the contents. All if -v9
 Will exit(0) if no error found in any file processed, otherwise exit(1)
 Project 'showlnk' started 20140201, update 20140204. current version 0.0.3

Note that it will also scan a directory for *.lnk files, accepts wild card, and 
multiple inputs...

Enjoy ;=))


Geoff R. McLane
reports _at_ geoffair _dot_ info

# eof
