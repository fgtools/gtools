/* main.c */

/*
    from : http://www.json.org/JSON_checker/
    This program demonstrates a simple application of JSON_checker. It reads
    a JSON text from STDIN, producing an error message if the text is rejected.

        % JSON_checker <test/pass1.json
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h> // for _strdup()

#include "JSON_checker.h"
#ifndef SPRTF
#define SPRTF printf
#endif

static const char *in_file = 0;
char *get_file_name(char *file)
{
    char *name = file;
    size_t len = strlen(file);
    size_t ii;
    int c;
    for (ii = 0; ii < len; ii++) {
        c = file[ii];
        if ((c == '\\')||(c == '/')) {
            if (file[ii+1])
                name = &file[ii+1];
        }
    }
    return name;
}

void give_help(char *name)
{
    SPRTF("%s [Options] in_json_file\n",get_file_name(name));
    SPRTF("Options:\n");
    SPRTF(" --help (-h or -?) = Show this help and exit(2)\n");
    SPRTF(" Given an json input file, check syntax, report errors, and\n");
    SPRTF(" exit(0) if ok, else exit(1)\n");
}

#define MX_PREV_CHARS 32
static char buf[MX_PREV_CHARS+4];

int main(int argc, char* argv[]) 
{
    JSON_checker jc;
    FILE *fp;
    int i, c, pc;
    int row = 0;
    int col = 0;
    char *arg, *sarg;
    int off = 0;
    int wrap = 0;

    if (argc < 2) {
        SPRTF("Error: No input file found in command!\n");
        give_help(argv[0]);
        return 1;
    }
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-') sarg++;
            c = *sarg;
            switch (c)
            {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            default:
                SPRTF("Unknown argument [%s]! Try -? aboirting exit(1)\n",arg);
                return 1;
            }

        } else {
            if (in_file) {
                SPRTF("Aready have input file %s! Can NOT accept a second %s\n", in_file, arg);
                return 1;
            }
            in_file = _strdup(arg);
        }
    }
    if (in_file == 0) {
        SPRTF("No input file found in command! Try -? aborting...\n");
        return 1;
    }
    fp = fopen(in_file,"r");
    if (!fp) {
        SPRTF("Unable to open file [%s]!\n", in_file);
        return 1;
    }
/*
    jc will contain a JSON_checker with a maximum depth of 50. (was 20)
*/
    jc = new_JSON_checker(50);

    pc = 0;
    off = 0;
    buf[off] = 0;
    while ( (c = fgetc(fp)) != EOF ) {
        if (c < ' ') {
            if ((c == '\r')||(c == '\n')) {
                if ((c == '\n') && (pc == '\r')) {
                    // forget this '\n'
                } else {
                    row++;
                    col = 0;
                }
            } else if (c == '\t') {
                col++;
            }
            buf[off++] = ' ';
        } else {
            col++;
            buf[off++] = (char)c;
        }
        buf[off] = 0;
        if (off >= MX_PREV_CHARS) {
            off = 0;
            wrap++;
        }
        if (!JSON_checker_char(jc, c)) {
            SPRTF("Lin=%d: Col=%d: JSON_checker_char: syntax error in file %s\n", row, col, in_file);
            SPRTF("Preceeding %2d chars: [",MX_PREV_CHARS);
            if (wrap) {
                if ((off + 1) < MX_PREV_CHARS) {
                    SPRTF("%s",&buf[off + 1]);
                }
            }
            SPRTF("%s",buf);
            SPRTF("]\n");
            off = 0;
            while ( (c = fgetc(fp)) != EOF ) {
                if (c < ' ') {
                    c = ' ';
                }
                buf[off++] = (char)c;
                if (off >= MX_PREV_CHARS) {
                    break;
                }
            }
            if (off) {
                buf[off] = 0;
                SPRTF("Following  %2d chars: [",off);
                SPRTF("%s",buf);
                SPRTF("]\n");
            }
            fclose(fp);
            return 1;
        }
        pc = c;
    }
    fclose(fp);
    if (!JSON_checker_done(jc)) {
        SPRTF("Lin=%d: Col=%d: JSON_checker_end: syntax errorin file %s\n", row, col, in_file);
        return 1;
    }
    return 0;
}